# Digitaler Zwilling eines Fischertechnik-Roboters
## Beschreibung
Dieses Projekt dienst der Simulation eines Roboters aus der fischertechnik-Lernfabrik. 
Programme, die für den Roboter geschrieben werden, können hiermit vor der realen Inbetriebnahme auf dem digitalen Zwilling getestet werden.

## Unity Version
Benötigte Unity Version: 2020.3.6f1

