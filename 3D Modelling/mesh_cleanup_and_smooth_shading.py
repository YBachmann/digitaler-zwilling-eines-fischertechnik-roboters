import bpy

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.remove_doubles()
bpy.ops.mesh.dissolve_limited()
bpy.ops.object.editmode_toggle()
bpy.ops.object.shade_smooth()
bpy.context.object.data.use_auto_smooth = True
