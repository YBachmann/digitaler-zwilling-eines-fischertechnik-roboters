﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : MonoBehaviour
{
    //Collision Manager instance. This value is initiated through unity editor.
    public CollisionManager CollisionManager;

    /// <summary>
    /// When a collision happens, this function will be called. We send the information about collision and objects to CollisionManager
    /// </summary>
    /// <param name="collision"> This parameter created when a collision is happend. 
    /// It hold information about collision and has reference to the object, which this object collieded into. </param>
    private void OnCollisionEnter(Collision collision)
    {
        CollisionManager.CollisionDetected(this.gameObject, collision); // Sending collision information to the collision manager.
    }

    /// <summary>
    /// When a collision ends, this function will be called. We send the information about collision and objects to CollisionManager
    /// </summary>
    /// <param name="collision"> This parameter created when a collision is happend. 
    /// It hold information about collision and has reference to the object, which this object collieded into. </param>
    private void OnCollisionExit(Collision collision)
    {
        CollisionManager.CollisionEnded(this.gameObject, collision); // Sending collision information to the collision manager.
    }
}
