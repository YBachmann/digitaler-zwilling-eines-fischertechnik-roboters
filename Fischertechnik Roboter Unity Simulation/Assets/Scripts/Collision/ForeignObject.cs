using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForeignObject : MonoBehaviour
{
    // inital material of this object.
    private Material _oldMaterial;
    //Collision Manager instance. This value is initiated through unity editor.
    public CollisionManager CollisionManager;
    // holds the MeshRenderer object, which we get from TryGetComponent method.
    private MeshRenderer _meshRenderer;

    // Warning material. Initiated through unity editor.
    public Material WarningMaterial;

    private const int _layerTransportObject = 10; // number of the unity layer "TransportObject"

    void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _oldMaterial = _meshRenderer.material;
    }
    /// <summary>
    /// When a collision happens, this function will be called. We send the information about collision and objects to CollisionManager
    /// </summary>
    /// <param name="collision"> This parameter created when a collision is happend. 
    /// It hold information about collision and has reference to the object, which this object collieded into. </param>
    private void OnCollisionEnter(Collision collision)
    {    
        // we dont need to give collision information between enviorement and transport objects.
        if (collision.gameObject.layer == _layerTransportObject)
        {
            return;
        }
        _meshRenderer.material = WarningMaterial;
        //Inform collision manager about the collision.
        CollisionManager.CollisionDetected(this.gameObject, collision);

    }
    /// <summary>
    /// This method will be called as long as this object collideds with an other.
    /// This method is needed because, when a object collides with two objects at the same time and when this object stops colliding
    /// with one of the objects, materila of this object still have to be warning material.
    /// </summary>
    /// <param name="collision"> This parameter created when a collision is happend. 
    /// It hold information about collision and has reference to the object, which this object collieded into. </param>
    private void OnCollisionStay(Collision collision)
    {
        // we dont need to give collision information between enviorement and transport objects.
        if (collision.gameObject.layer == _layerTransportObject)
        {
            return;
        }


        if(_meshRenderer.material != WarningMaterial)
        {
           _meshRenderer.material = WarningMaterial;
        }
        return;
    }
    /// <summary>
    /// When a collision ends, this function will be called. We send the information about collision and objects to CollisionManager
    /// </summary>
    /// <param name="collision"> This parameter created when a collision is happend. 
    /// It hold information about collision and has reference to the object, which this object collieded into. </param>
    private void OnCollisionExit(Collision collision)
    {
        // we dont need to give collision information between enviorement and transport objects.
        if (collision.gameObject.layer == _layerTransportObject)
        {
            return;
        }

        //Inform collision manager about the ended collision.
        CollisionManager.CollisionEnded(this.gameObject, collision); // Sending collision information to the collision manager.

        // Setting old materila back.
        _meshRenderer.material = _oldMaterial;
    }

}
