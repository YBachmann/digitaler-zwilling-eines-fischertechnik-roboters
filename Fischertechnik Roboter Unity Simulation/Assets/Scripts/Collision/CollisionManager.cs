﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CollisionManager : MonoBehaviour
{
    [SerializeField]
    private Simulation _simulation;


    public Camera CollisionCamera;  // Camera that shows where the collision is.
    private Vector3 _positionOfCollision; // This variable holds the coordinates in which the collision is.
    private Vector3 _posCamera; // Position of the Collision Camera.
    private Vector3 _initialCamPos; // Initial position of the collision camera.
    private Quaternion _initialCamRot; // Initial rotation of the collision camera.

    [SerializeField]
    private float _cameraSpeed = 200; // Change this if you want to move collision camera faster or slower.

    [SerializeField]
    private float _cameraRotationSpeed = 45; // Change this if you want to rotate collision camera faster or slower.
    private bool _moveCamera = false; // When it is true, collision camera starts his movement.

    private RaycastHit _rayInfo; // This variable holds information about Ray when it hits.
    private float _distance; // Distance between collision camera and collision point.
    private Material _materialOfObjRayHits;

    private bool _stageOne = true; // true = start stage one, false = stage one is over. 
    private bool _stageTwo = false; // true = start stage two, false = stage two is over. 
    private float _timer = 3; // This timer is for rotation of the coliision camera. It holds how many seconds the collision camera will turn.

    private string _firstCollisionObjName; // Name of the first object that collides.
    private string _secondCollisionObjName; // Name of the second object that collides.

    // UI element of collision warnings.
    public GameObject CollisionWarningSign; // Warning Sign on Collision Tab.
    public GameObject CollisionWarningPanel; //Activates when a collision happens.
    public TMP_Text ColInfoText; // UI element that holds the information about collision. 

    public Material WarningMaterial; // When a collision happens, collided object changes its material to WarningMaterial.
                                     // In Update() we use this material to check if camera is seeing collided objects.

    private Logger _logObj = Logger.Instance;   //The information about the collision will be send to Logger through this logger instance.

    /// <summary>
    /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">HERE</see>
    /// </summary>
    private void Start() 
    {
        _posCamera = CollisionCamera.transform.position; 
        _initialCamPos = CollisionCamera.transform.position;
        _initialCamRot = CollisionCamera.transform.rotation;

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html">HERE</see>
    /// </summary>
    private void Update()
    {

        if (_moveCamera) // Checks if collision camera is allowed to move.
        {
            // direction vector for moving collision camera.
            Vector3 direction = new Vector3();

            //getting new position of the camera after it moves to check the distance of the camera and collision point.
            _posCamera = CollisionCamera.transform.position;

            // Calculating distance
            _distance = Mathf.Sqrt(Mathf.Pow((_posCamera.x - _positionOfCollision.x), 2) 
                                 + Mathf.Pow((_posCamera.y - _positionOfCollision.y), 2)
                                 + Mathf.Pow((_posCamera.z - _positionOfCollision.z), 2));
            // setting direction.
            direction = (_positionOfCollision - CollisionCamera.transform.position) / _distance; 

            // stage one starts
            if (_stageOne)
            {
                // camera moves towards to collision point.
                CollisionCamera.transform.Translate(direction * Time.deltaTime * _cameraSpeed);

                // Camera looks at the collision point.
                CollisionCamera.transform.LookAt(_positionOfCollision); 

                // When the distance smaller than 75 unit, seconds stage begins.
                if (_distance <= 0.3) 
                {
                    _stageTwo = true;
                    _stageOne = false;
                }

            }
            
            // Stage two begins
            if (_stageTwo) 
            {
  
                // rotates camera around collision point in Y Axis.
                CollisionCamera.transform.RotateAround(_positionOfCollision, new Vector3(0, -1, 0), _cameraRotationSpeed * Time.deltaTime); 

                // This ray is sended from collision camera to collision point to check if there is a obstacle between camera and collision point.
                Ray ray = new Ray(CollisionCamera.transform.position, direction);
                // Sends ray. if ray hitted something it returns true.
                if (Physics.Raycast(ray, out _rayInfo, 200)) 
                {
                    
                    MeshRenderer meshRendererOfObj;
                    if(_rayInfo.collider.gameObject.TryGetComponent<MeshRenderer>(out meshRendererOfObj))
                    {
                        _materialOfObjRayHits = _rayInfo.collider.gameObject.GetComponent<MeshRenderer>().material;
                    }
                    // if ray hits one of the collided object, 
                    // that means we can see the objcet therefore no need to rotate camera anylonger.
                    // However, the camera rotates for the specified time just in case.
                    if (((_rayInfo.collider.gameObject.name == _firstCollisionObjName) || _materialOfObjRayHits == WarningMaterial )  && (_timer <= 0)) // || (_rayInfo.collider.gameObject.name == _secondCollisionObjName))
                    {                                                                                                                                   
                                                                                                                                                        
                        _stageOne = true;
                        _stageTwo = false;
                        // Camera no longer needs to move.
                        _moveCamera = false;

                        // Set to timer back again to inital value to use later.
                        _timer = 3; 
                    }

                }
                else
                {
                    if (_timer <= 0)
                    {
                        _stageOne = true;
                        _stageTwo = false;
                        // Camera no longer needs to move.
                        _moveCamera = false;

                        // Set to timer back again to inital value to use later.
                        _timer = 3;
                    }
                }
                // decreases the timer.
                _timer -= Time.deltaTime; 

            }

        }
    }
    /// <summary>
    /// Changes the trigger status in RoboStatus when a collision starts. Will be called from Sensor class and ForeignObject class.
    /// </summary>
    /// <param name="obj">The motor instructions. 
    /// thats the object, that detected the collision. this object calls this function </param>
    /// /// <param name="collision"> collision object is created when a collision happens. 
    /// this object hold information about collision and has a reference to object, which we collided into. </param>
    public void CollisionDetected(GameObject obj, Collision collision) 
    {             
        switch (collision.gameObject.tag) // checking if the collision  an expected or an unexpected collision is and which sensor is collided.
        {
            case "HorizontalSensor":
                // sending stop signal.
                RoboStatus.Instance.SetTriggerStatus(3, true);
                _logObj.UiLog("Horizontal sensor is pushed.", LogLevel.Info); // Sending information about sensors to Logger.
                _logObj.InfoLog("Horizontal sensor is pushed.", LogLevel.Info);

                break;

            case "VerticalSensor":
                // sending stop signal.
                RoboStatus.Instance.SetTriggerStatus(2, true);
                _logObj.UiLog("Vertical sensor is pushed.", LogLevel.Info);
                _logObj.InfoLog("Vertical sensor is pushed.", LogLevel.Info);

                break;

            case "RotationSensor":
                // sending stop signal.
                RoboStatus.Instance.SetTriggerStatus(1, true);
                _logObj.UiLog("Rotation sensor is pushed.", LogLevel.Info);
                _logObj.InfoLog("Rotation sensor is pushed.", LogLevel.Info);

                break;

            // The Robot have collided with a foreign object. That means we have to stop all movement because damage is unpredictable
            case "ForeignObject":
                
                if (RoboStatus.Instance.GetOnlineStatus())
                {
                    _simulation.EmergencyBrake();
                    // starts the camera movement. 
                    _positionOfCollision = obj.transform.position;
                    _moveCamera = true;
                    _stageOne = true;

                    CollisionWarningPanel.SetActive(true);
                    // displaying collision information.
                    _firstCollisionObjName = obj.name;
                    _secondCollisionObjName = collision.gameObject.name;
                    ColInfoText.text = "Collision Position: " + "\n" + _positionOfCollision.ToString() + "\n"
                                     + "Collision Time:\n" + "After " + Time.realtimeSinceStartup.ToString() + " seconds" + "\n"
                                     + "Collision Objects: " + "\n" + "-" + _secondCollisionObjName + "\n-" + _firstCollisionObjName;

                    _logObj.InfoLog("Unexpected collision happend at " + _positionOfCollision.ToString() + ", Objects: " + _firstCollisionObjName + ", " + _secondCollisionObjName, LogLevel.Warn);  // sending information to the Logger.
                    CollisionWarningSign.SetActive(true);
                    
                }

                break;
               
                // When arm of the robot overextends we stop the movement.
            case "OverExtendHorizontal":
                RoboStatus.Instance.SetOverExtendStatus(3, true);
                _simulation.EmergencyBrake();
                _logObj.InfoLog("Arm of the crane dangerously extended! Time: "+ Time.realtimeSinceStartup.ToString()+" after", LogLevel.Warn);
                ColInfoText.text = "Arm of the crane dangerously extended!";
                CollisionWarningSign.SetActive(true);
                break;
            case "OverExtendVertical":
                RoboStatus.Instance.SetOverExtendStatus(2, true);
                _simulation.EmergencyBrake();
                _logObj.InfoLog("Arm of the crane is dangerously low! Time: " + Time.realtimeSinceStartup.ToString() + " after", LogLevel.Warn);
                ColInfoText.text = "Arm of the crane is dangerously low!";
                CollisionWarningSign.SetActive(true);
                break;
            case "OverExtendRotation":
                RoboStatus.Instance.SetOverExtendStatus(1, true);
                _simulation.EmergencyBrake();
                _logObj.InfoLog("Arm of the Crane reached rotation limit! Time: " + Time.realtimeSinceStartup.ToString() + " after", LogLevel.Warn);
                ColInfoText.text = "Arm of the Crane reached rotation limit!";
                CollisionWarningSign.SetActive(true);
                break;
        }
        
    }
    /// <summary>
    /// Changes the trigger status in RoboStatus when a collision ends. Will be called from Sensor class and ForeignObject class.
    /// </summary>
    /// <param name="obj">The motor instructions. 
    /// thats the object, that detected the collision end. this object calls this function </param>
    /// /// <param name="collision"> collision object is created when a collision happens. 
    /// this object hold information about collision and has a reference to object, which we collided into. </param>
    public void CollisionEnded(GameObject obj, Collision collision)
    {
        switch (collision.gameObject.tag) 
        {
            // checking if the collision an expected or an unexpected collision was and which sensor was collided.
            case "HorizontalSensor":
                // sending can move again signal.
                RoboStatus.Instance.SetTriggerStatus(3, false);

                //Sending collision information to Logger.
                _logObj.UiLog("Horizontal sensor is free.", LogLevel.Info);
                _logObj.InfoLog("Horizontal sensor is free.", LogLevel.Info);

                break;

            case "VerticalSensor":
                // sending can move again signal.
                RoboStatus.Instance.SetTriggerStatus(2, false);

                //Sending collision information to Logger.
                _logObj.UiLog("Vertical sensor is free.", LogLevel.Info);
                _logObj.InfoLog("Vertical sensor is free.", LogLevel.Info);

                break;

            case "RotationSensor":
                // sending can move again signal.
                RoboStatus.Instance.SetTriggerStatus(1, false);

                //Sending collision information to Logger.
                _logObj.UiLog("Rotation sensor is free.", LogLevel.Info);
                _logObj.InfoLog("Rotation sensor is free.", LogLevel.Info);

                break;

            case "ForeignObject":

                if (RoboStatus.Instance.GetOnlineStatus())
                {
                    CollisionCamera.transform.position = _initialCamPos; // setting the collision camera back to its initial position.
                    CollisionCamera.transform.LookAt(new Vector3(0, 0, 0)); // camera should always look at the robot.
                    _moveCamera = false;
                    _stageOne = false;
                    _stageTwo = false;
                }
                
                break;

            case "OverExtendHorizontal":
                RoboStatus.Instance.SetOverExtendStatus(3, false);
                break;
            case "OverExtendVertical":
                RoboStatus.Instance.SetOverExtendStatus(2, false);
                break;
            case "OverExtendRotation":
                RoboStatus.Instance.SetOverExtendStatus(1, false);
                break;
        }

    }
    /// <summary>
    /// Closes the collision warnings ui elements. WarningCloseButton calls this method.
    /// sets camera in its initial position, so that it can be used for later collisions.
    /// </summary>
    public void SetBackCamera() // 
    {
        _moveCamera = false;

        //Sets Camera its initial position.
        CollisionCamera.transform.position = _initialCamPos;
        CollisionCamera.transform.rotation = _initialCamRot;
        
    }

    /// <summary>
    /// This method stop rotation(stage two) of the collision camera.
    /// </summary>
    public void StopCameraRotation()
    {
        _stageTwo = false;
    }

}

