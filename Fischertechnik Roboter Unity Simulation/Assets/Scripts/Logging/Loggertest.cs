using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loggertest : MonoBehaviour
{
    private Logger _logObj;
    private RoboStatus _roboStatus;

    void Start()
    {
        _logObj = Logger.Instance;
        _roboStatus = RoboStatus.Instance;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            Debug.Log("Logtest schreiben");
            
            string txt = "loggertest";

            _logObj.CombinedLog(txt, LogLevel.Fail);


        }
    }
}
