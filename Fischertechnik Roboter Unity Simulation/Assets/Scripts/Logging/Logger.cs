using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;

/// <summary>
/// Receive calls from everywhere to send log messages to different locations. 
/// </summary>
/// <param name="logLevel">Level of the log, defined by the enum LogLevel</param>
public interface ILogger
{
    void InfoLog(string txt, LogLevel loglevel);

    void UiLog(string txt, LogLevel logLevel);

    void CombinedLog(string txt, LogLevel loglevel);

    void SetLoglevel(LogLevel loglevel);
}

/// <summary>
/// Enum for different Loglevels. 
/// </summary>
/// <param name="Info">Confirmation that things are working as expected.</param>
/// <param name="Warn">An indication that something unexpected happened, or indicative of some problem in the near future (e.g. ‘disk space low’). The software is still working as expected.</param>
/// <param name="Debug">Detailed information, typically of interest only when diagnosing problems.</param>
/// <param name="Error">Due to a more serious problem, the software has not been able to perform some function.</param>
/// <param name="Fail">Used for logging when something went wrong. A serious error, indicating that the program itself may be unable to continue running.</param>
public enum LogLevel
{
    // Do NOT change order of loglevel!
    Debug,
    Info,
    Warn,
    Error,
    Fail
}

public class Logger : ILogger
{
    [SerializeField]
    public LogLevel LogLevelGlobal { get; private set; } = LogLevel.Info;

    [SerializeField]
    private String _path = @".\LogFile";

    [SerializeField]
    private int _filenumber = 2;

    [SerializeField]
    private int _filecount = 0;

    private int _delete_file_count = 1;
    private static Logger _instance = null;
    private static readonly object _padlock = new object();
    // private RoboStatus _roboStatus;

    public String uiLogString = "";

    static readonly object _fileLock = new object();

    private Logger()
    {
        // _roboStatus = RoboStatus.Instance;
    }

    /// <summary>
    /// This is not a function but an instance declaration that directly implements a get()-function
    /// The get-function creates/returns an instance of Logger. It is encapsules inside a lock() to ensure thread safety. 
    /// </summary>
    /// <value>
    /// The instance.
    /// </value>
    public static Logger Instance
    {
        get
        {
            lock (_padlock)
            {
                if (_instance == null)
                {
                    _instance = new Logger();
                }
                return _instance;
            }
        }
    }

    /// <summary>
    /// Receive calls from everywhere to send log messages to a .txt-file. 
    /// </summary>
    /// <param name="logLevel">Level of the log, defined by the enum LogLevel</param>
    /// <param name="txt">String, to convert into message together with the Loglevel</param>
    /// <param name="path">Location, where to save the .txt-file</param>
    /// <param name="text">String, to send into .txt-file</param>
    public void InfoLog(string txt, LogLevel loglevel)
    {
        if (loglevel >= LogLevelGlobal)
        {
            lock (_fileLock)
            {
                string text = txt + ": " + loglevel + Environment.NewLine;

                String file_name = @"" + _path + (_filecount + 1) + ".txt";
                FileInfo fi = new FileInfo(file_name);

                if (!File.Exists(file_name))
                {
                    File.AppendAllText(file_name, text);
                }
                else if (fi.Length <= 5000) //size in bytes -> 5 000 000 bytes = 5 MB
                {
                    File.AppendAllText(file_name, text);
                }
                else
                {
                    File.AppendAllText(file_name, text);
                    if ((_filecount + 1) > _filenumber)          // Check, if NEXT file is greater than MAX filenumber
                    {
                        _filecount = 0;                         // Set global filecount back to 0 and begin with file 1
                        File.Delete(@"" + _path + (_filecount + 1) + ".txt");
                    }
                    else
                    {
                        File.Delete(@"" + _path + (_filecount + 2) + ".txt");
                        _filecount++;                           // Delete NEXT file and increment filecount
                    }
                }
            }
        }
    }

    /// <summary>
    /// Receive calls from everywhere to send log messages to UI. 
    /// </summary>
    /// <param name="logLevel">Level of the log, defined by the enum LogLevel</param>
    /// <param name="level">Level of the log, converted into string</param>
    /// <param name="txt">String, to convert into message together with the Loglevel</param>
    /// <param name="updateUiLog">List, to send to roboStatus</param>
    public void UiLog(string txt, LogLevel loglevel)
    {
        if (loglevel >= LogLevelGlobal)
        {
            String level = loglevel.ToString();
            List<Dictionary<string, string>> updateUiLog = new List<Dictionary<string, string>>();
            updateUiLog.Add(new Dictionary<string, string>(){
                {"loglevel", ((int)loglevel).ToString()},
                {"text", txt}
            });

            RoboStatus.Instance.updateUiLog(updateUiLog);
        }
    }

    /// <summary>
    /// Combine both logcalls with one method. 
    /// </summary>
    /// <param name="logLevel">Level of the log, defined by the enum LogLevel</param>
    /// <param name="txt">String, to convert into message together with the Loglevel</param>
    public void CombinedLog(string txt, LogLevel loglevel)
    {
        InfoLog(txt, loglevel);
        UiLog(txt, loglevel);
    }

    public void SetLoglevel(LogLevel loglevel)
    {
        CombinedLog("Set Log Level to" + loglevel.ToString(), LogLevel.Info);
        LogLevelGlobal = loglevel;
    }
}
