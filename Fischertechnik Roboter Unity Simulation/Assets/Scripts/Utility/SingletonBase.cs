using System;

/// <summary>
/// Generic thread-safe abstract singleton class
/// References: 
/// https://stackoverflow.com/questions/16745629/how-to-abstract-a-singleton-class
/// https://docs.microsoft.com/de-de/dotnet/framework/performance/lazy-initialization
/// https://docs.microsoft.com/de-de/dotnet/api/system.activator.createinstance?view=net-5.0#System_Activator_CreateInstance_System_Type_System_Boolean_
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class SingletonBase<T> where T : SingletonBase<T>
{
    private static readonly Lazy<T> Lazy =
        new Lazy<T>(() => Activator.CreateInstance(typeof(T), true) as T);

    public static T Instance => Lazy.Value;
}
