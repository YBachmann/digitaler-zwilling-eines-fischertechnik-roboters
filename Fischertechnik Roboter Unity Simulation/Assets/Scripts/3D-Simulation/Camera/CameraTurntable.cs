using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraTurntable : MonoBehaviour
{
    [SerializeField]
    private GameObject _camera;
    [SerializeField]
    [Range(20, 200)]
    private float _rotationSpeed = 75f;
    [SerializeField]
    [Range(-30, 30)]
    private int _minAngle = -10;
    [SerializeField]
    [Range(40, 85)]
    private int _maxAngle = 80;
    [SerializeField]
    [Range(0.002f, 0.2f)]
    private float _translationSpeed = 0.1f;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">HERE</see>
    /// </summary>
    void Start()
    {
        // point camera view at camera center
        _camera.transform.LookAt(transform.position);

        // redundantly check _maxAngle since this MUST NOT exceed 85 degrees as controlling the camera could become unintuitive.  
        if (_maxAngle > 85)
        {
            Logger.Instance.CombinedLog("'_maxAngle' for CameraTurntable should exceeded 85 degrees. Automatically set to 85. ", LogLevel.Warn);
            _maxAngle = 85;
        }
    }

    /// <summary>
    /// Rotates camera.
    /// </summary>
    private void RotateCamera()
    {
        // Get mouse inputs
        float rotationHorizontal = Input.GetAxis("Mouse X") * _rotationSpeed * Mathf.Deg2Rad;
        float rotationVertical = -Input.GetAxis("Mouse Y") * _rotationSpeed * Mathf.Deg2Rad;

        // Make horizontal movement (always allowed)
        _camera.transform.RotateAround(this.transform.position, Vector3.up, rotationHorizontal);

        // We can get the angle from the camera to the ground plane easily considering 'Vector3.up' is the normal-vector of the ground plane. 
        float currentAngleToGroundPlane = 90 - Vector3.Angle(Vector3.up, _camera.transform.position - this.transform.position);

        // Don't move above _maxAngle or below _minAngle
        if ((currentAngleToGroundPlane > _maxAngle && rotationVertical > 0)
            || (currentAngleToGroundPlane < _minAngle && rotationVertical < 0))
        {
            return;
        }
        // Rotation axis for vertical movement is the vector that is perpendicular to the vector from the camera-center(this) to the camera and the UP-Axis. 
        Vector3 verticalRotationAxis = Vector3.Cross(_camera.transform.position - this.transform.position, Vector3.up).normalized;
        _camera.transform.RotateAround(this.transform.position, Vector3.Cross(_camera.transform.position - this.transform.position, Vector3.up), rotationVertical);
    }

    /// <summary>
    /// Translates camera by moving the camera center (the camera is a child of the camera center and thus gets translated with it).
    /// </summary>
    private void TranslateCamera()
    {
        // Get mouse inputs
        float translationHorizontal = -Input.GetAxis("Mouse X") * _translationSpeed;
        float translationVertical = -Input.GetAxis("Mouse Y") * _translationSpeed;

        // Build translation vector by multiplying the mouse movement with the current camera view direction
        Vector3 translationVector = _camera.transform.up.normalized * translationVertical + _camera.transform.right.normalized * translationHorizontal;

        // Move camera center (and camera)
        this.transform.Translate(translationVector);
    }

    /// <summary>
    /// Dragging the mouse rotates the camera. Holding down Left Control while dragging translates the camera.
    ///
    /// OnMouseDrag is called when the user has clicked on a Collider and is still holding down the mouse.
    /// OnMouseDrag is called every frame while the mouse is down.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnMouseDrag.html">HERE</see>
    /// </summary>
    void OnMouseDrag()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                TranslateCamera();
            }
            else
            {
                RotateCamera();
            }
        }
    }

}
