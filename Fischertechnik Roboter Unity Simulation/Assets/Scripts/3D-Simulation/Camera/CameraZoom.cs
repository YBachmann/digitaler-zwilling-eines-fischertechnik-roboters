using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour
{
    [SerializeField]
    [Range(5f, 25f)]
    private float _minFov = 10f;
    [SerializeField]
    [Range(40f, 90f)]
    private float _maxFov = 80f;
    [SerializeField]
    [Range(5f, 50f)]
    private float _sensitivity = 15f;


    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html">HERE</see>
    /// </summary>
    void Update()
    {
        float fov = Camera.main.fieldOfView;
        fov -= Input.GetAxis("Mouse ScrollWheel") * _sensitivity;
        fov = Mathf.Clamp(fov, _minFov, _maxFov);
        Camera.main.fieldOfView = fov;
    }
}
