using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class ValveEventListener : MonoBehaviour, IEventListener
{
    [SerializeField]
    private Valve _valve;

    private Logger _logger;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">HERE</see>
    /// </summary>
    void Start()
    {
        _logger = Logger.Instance;

        if (_valve == null)
        {
            _logger.CombinedLog("ValveEventListener '" + gameObject.name + "' does not have a reference to a valve set. This eventListener will have no effect therefore. "
                        + "Please select a vacuum pump in the inspector window. ", LogLevel.Warn);
        }

        RoboStatus.Instance.eventPublisher.subscribe(EventPublisher.EventType.ValveInstruction, this);
    }

    /// <summary>
    /// Decode json data and send instructions to vacuum pump. 
    /// </summary>
    /// <param name="jsonData">The json data.</param>
    public void UpdateListener(string jsonData)
    {
        if (_valve == null)
        {
            return;
        }

        // convert json back to list of dicts
        System.Tuple<bool, int> valveInstructions = JsonConvert.DeserializeObject<System.Tuple<bool, int>>(jsonData);

        // check if list of instructions contain one for this Motor
        if (valveInstructions.Item2 == _valve.ValveId)
        {
            _valve.SetState(valveInstructions.Item1);
        }
    }
}
