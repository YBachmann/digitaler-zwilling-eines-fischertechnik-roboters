using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class VacPumpEventListener : MonoBehaviour, IEventListener
{
    [SerializeField]
    private VacPump _vacPump;

    private Logger _logger;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">HERE</see>
    /// </summary>
    void Start()
    {
        _logger = Logger.Instance;

        if (_vacPump == null)
        {
            _logger.CombinedLog("VacPumpEventListener '" + gameObject.name + "' does not have a reference to a VacPump set. This eventListener will have no effect therefore. "
                        + "Please select a vacuum pump in the inspector window. ", LogLevel.Warn);
        }

        RoboStatus.Instance.eventPublisher.subscribe(EventPublisher.EventType.VacPumpInstruction, this);
    }

    /// <summary>
    /// Decode json data and send instructions to vacuum pump. 
    /// </summary>
    /// <param name="jsonData">The json data.</param>
    public void UpdateListener(string jsonData)
    {
        if (_vacPump == null)
        {
            return;
        }

        // convert json back to list of dicts
        System.Tuple<bool, int> VacPumpInstructions = JsonConvert.DeserializeObject<System.Tuple<bool, int>>(jsonData);

        // check if list of instructions contain one for this Motor
        if (VacPumpInstructions.Item2 == _vacPump.VacPumpId)
        {
            _vacPump.setPumpActive(VacPumpInstructions.Item1);
        }
    }
}
