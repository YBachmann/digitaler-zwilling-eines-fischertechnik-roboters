using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(FixedJoint))]
public class Gripper : PneumaticComponent
{
    private Collider _grippedObject;
    private FixedJoint _joint;

    private const int _jointBrakeForce = 10;
    private const int _jointBrakeTorque = 10;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">HERE</see>
    /// </summary>
    void Start()
    {
        CreateJoint();
    }

    /// <summary>
    /// Sets input pressure of the gripper.
    /// </summary>
    /// <param name="pressure">The specified input pressure.</param>
    public override void SetInputPressure(PressureState pressure)
    {
        _inputPressure = pressure;
        _logger.CombinedLog("Input pressure of gripper set to " + _inputPressure.ToString(), LogLevel.Debug);
        if (_inputPressure == PressureState.ReleasePressure
            || _inputPressure == PressureState.PositivePressure)
        {
            ReleaseObject();
        }
    }


    /// <summary>
    /// Updates the input pressure of the next pneumatic component.
    /// </summary>
    /// <param name="pressure">The pressure (optional, not used in this class but in sister classes).</param>
    public override void UpdateNextComponentInput(PressureState pressure = PressureState.Undefined)
    {
        _logger.CombinedLog("PneumaticComponent of Type 'Gripper' can't update next component input!", LogLevel.Error);
    }

    /// <summary>
    /// Creates a fixed joint that the grippable object will get connected to. 
    /// Unity destroys the joint instance if the joint brakes so this function will get used every time the joint brakes. 
    /// </summary>
    private void CreateJoint()
    {
        _joint = gameObject.GetComponent<FixedJoint>();
        if (_joint == null)
        {
            _joint = gameObject.AddComponent<FixedJoint>();
        }
        _joint.enableCollision = false;
        _joint.enablePreprocessing = true;
        _joint.breakForce = _jointBrakeForce;
        _joint.breakTorque = _jointBrakeTorque;
    }

    /// <summary>
    /// Try to grip an object
    /// </summary>
    /// <param name="coll">The collider object</param>
    private void GripObject(Collider coll)
    {
        if (_inputPressure == PressureState.NegativePressure && _grippedObject == null)
        {
            if (coll.CompareTag("grippable") == true)
            {
                // Grip object
                _grippedObject = coll;
                _grippedObject.gameObject.transform.parent = this.gameObject.transform;
                _grippedObject.attachedRigidbody.useGravity = false;
                if (_joint == null)
                {
                    CreateJoint();
                }
                _joint.connectedBody = _grippedObject.GetComponent<Rigidbody>();
            }
            _logger.CombinedLog("Gripper just gripped an object!", LogLevel.Info);
        }
    }

    /// <summary>
    /// Release the gripped object
    /// </summary>
    private void ReleaseObject()
    {
        if (_grippedObject != null)
        {
            _grippedObject.gameObject.transform.parent = null;
            _grippedObject.attachedRigidbody.useGravity = true;
            _grippedObject = null;
            // If the joint brakes unity destroys its instance so check for ==null to be safe. 
            if (_joint)
            {
                _joint.connectedBody = null;
            }
            _logger.CombinedLog("Gripper just released an object!", LogLevel.Info);
        }
    }

    /// <summary>
    /// OnTriggerStay is called once per physics update for every Collider 'coll' that is touching the trigger.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnTriggerStay.html">HERE</see>
    /// </summary>
    void OnTriggerStay(Collider coll)
    {
        GripObject(coll);
    }

    /// <summary>
    /// Called when a joint attached to the same game object broke.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnJointBreak.html">HERE</see>
    /// </summary>
    void OnJointBreak(float breakForce)
    {
        // If the joint brakes unity destroys its instance so we have to create a new one now
        CreateJoint();
        ReleaseObject();
        _logger.CombinedLog("Gripper joint has just been broken! Force: " + breakForce, LogLevel.Debug);
    }

}
