using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class VacPump : PneumaticComponent
{
    public const int DefaultVacPumpId = -1;
    public int VacPumpId { get { return _vacPumpId; } private set { _vacPumpId = value; } }
    [SerializeField]
    private int _vacPumpId = DefaultVacPumpId;

    public bool Active { get; private set; } = false;

    [SerializeField]
    private AudioSource _audioDataPumpSound;
    private bool _startPumpSoundInNextFrameUpdate = false;
    private bool _stopPumpSoundInNextFrameUpdate = false;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">HERE</see>
    /// </summary>
    void Start()
    {
        RoboStatus.Instance.RegisterVacPump(this);
        _audioDataPumpSound = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html">HERE</see>
    /// </summary>
    void Update()
    {
        // Playing and stopping the audio has to be decoupled this way because otherwise it blocks the server thread. 
        // You can't just call Play() or Stop() in a new thread because of constraints of the Unity framework those funcions can only be called from the main thread.
        if (_startPumpSoundInNextFrameUpdate)
        {
            _startPumpSoundInNextFrameUpdate = false;
            _audioDataPumpSound.Play();
        }
        else if (_stopPumpSoundInNextFrameUpdate)
        {
            _stopPumpSoundInNextFrameUpdate = false;
            _audioDataPumpSound.Stop();
        }
    }


    /// <summary>
    /// Sets input pressure of the pump.
    /// Do not use this function. It only exists because this is a subclass of 'PneumaticComponent'.
    /// </summary>
    /// <param name="pressure">The specified input pressure.</param>
    public override void SetInputPressure(PressureState pressure)
    {
        _logger.CombinedLog("Input pressure can't be set for PneumaticComponent of Type 'VacPump'!", LogLevel.Error);
    }

    /// <summary>
    /// Updates the input pressure of the next pneumatic component.
    /// </summary>
    /// <param name="pressure">The pressure (optional, not used in this class but in sister classes).</param>
    public override void UpdateNextComponentInput(PressureState pressure = PressureState.Undefined)
    {
        if (_nextComponent)
        {
            if (Active)
            {
                _nextComponent.SetInputPressure(PressureState.PositivePressure);
            }
            else
            {
                _nextComponent.SetInputPressure(PressureState.HoldPressure);
            }
        }
    }

    /// <summary>
    /// Sets pump state (active/inactive).
    /// </summary>
    /// <param name="active">Pump active state.</param>
    public void setPumpActive(bool active)
    {
        _logger.CombinedLog("VacPump active set to " + active, LogLevel.Info);
        Active = active;
        if (Active)
        {
            _startPumpSoundInNextFrameUpdate = true;
        }
        else
        {
            _stopPumpSoundInNextFrameUpdate = true;
        }
        UpdateNextComponentInput();
    }
}
