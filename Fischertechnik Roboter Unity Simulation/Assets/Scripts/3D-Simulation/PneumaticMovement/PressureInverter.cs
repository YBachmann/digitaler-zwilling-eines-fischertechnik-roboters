using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PressureInverter : PneumaticComponent
{
    public enum PistonState
    {
        Undefined,
        Retracted,
        Extended,
        Retracting,
        Extending
    }

    [SerializeField]
    private Vector3 _extendPistonDirection;
    [SerializeField]
    private const float _extendPistonDistanceInMeters = 0.027f; // 0.027m = 2.7cm
    private Vector3 _retractedPosition;
    private PistonState _currentPistonState;
    private PistonState _lastPistonState;
    [SerializeField]
    private float _extendingSpeed;
    [SerializeField]
    private float _retractingSpeed;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">HERE</see>
    /// </summary>
    void Start()
    {
        // Save initial position of retracted piston 
        _retractedPosition = transform.position;
        _extendPistonDirection = _extendPistonDirection.normalized;

        _currentPistonState = PistonState.Retracted;
        _lastPistonState = PistonState.Undefined;
    }

    /// <summary>
    /// Run piston state machine.
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html">HERE</see>
    /// </summary>
    void Update()
    {
        if (_lastPistonState != _currentPistonState)
        {
            _logger.CombinedLog("PressureInverter state set to: " + _currentPistonState.ToString(), LogLevel.Debug);
            Entry();
            _lastPistonState = _currentPistonState;
        }
        // Note: The current piston state should only be changed in Run()
        Run();
        if (_lastPistonState != _currentPistonState)
        {
            Exit();
        }
    }

    /// <summary>
    /// Sets input pressure of the pressure inverter.
    /// </summary>
    /// <param name="pressure">The specified input pressure.</param>
    public override void SetInputPressure(PressureState pressure)
    {
        _inputPressure = pressure;
        _logger.CombinedLog("Input pressure of pressure inverter set to " + _inputPressure.ToString(), LogLevel.Debug);
    }

    /// <summary>
    /// Updates the input pressure of the next pneumatic component.
    /// </summary>
    /// <param name="pressure">The pressure (optional).</param>
    public override void UpdateNextComponentInput(PressureState pressure = PressureState.Undefined)
    {
        if (_nextComponent)
        {
            // Set input pressure of next component determined by the internal state machine of the PressureInverter. 
            _nextComponent.SetInputPressure(pressure);
        }
    }

    /// <summary>
    /// Entry function of piston state machine.
    /// </summary>
    private void Entry()
    {
        switch (_currentPistonState)
        {
            case PistonState.Retracted:
                UpdateNextComponentInput(PressureState.HoldPressure);
                break;
            case PistonState.Extended:
                UpdateNextComponentInput(PressureState.HoldPressure);
                break;
            case PistonState.Retracting:
                UpdateNextComponentInput(PressureState.PositivePressure);
                break;
            case PistonState.Extending:
                UpdateNextComponentInput(PressureState.NegativePressure);
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Run function of piston state machine.
    /// </summary>
    private void Run()
    {
        switch (_currentPistonState)
        {
            case PistonState.Retracted:
                if (_inputPressure == PressureState.PositivePressure)
                {
                    _currentPistonState = PistonState.Extending;
                }
                break;
            case PistonState.Extended:
                if (_inputPressure == PressureState.ReleasePressure
                    || _inputPressure == PressureState.NegativePressure)
                {
                    _currentPistonState = PistonState.Retracting;
                }
                break;
            case PistonState.Retracting:
                // TODO: check for changes of _inputPressure while the piston is moving
                if (!MovePiston(false))
                {
                    _currentPistonState = PistonState.Retracted;
                }
                break;
            case PistonState.Extending:
                // TODO: check for changes of _inputPressure while the piston is moving
                if (!MovePiston(true))
                {
                    _currentPistonState = PistonState.Extended;
                }
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Exit function of piston state machine.
    /// Currently doesn't do anything but still here for completeness.
    /// </summary>
    private void Exit()
    {
        switch (_currentPistonState)
        {
            case PistonState.Retracted:
                break;
            case PistonState.Extended:
                break;
            case PistonState.Retracting:
                break;
            case PistonState.Extending:
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Moves the piston (extend/retract) within the specified boundaries.
    /// </summary>
    /// <param name="extend">True if the piston should be extended, false if it should be retracted.</param>
    /// <returns>A bool beeing false if the piston is fully retracted or extended. </returns>
    private bool MovePiston(bool extend)
    {
        if (extend)
        {
            Vector3 newPosition = Vector3.MoveTowards(transform.position, _retractedPosition + (_extendPistonDirection * _extendPistonDistanceInMeters), _extendingSpeed * Time.deltaTime);
            if (newPosition == transform.position)
            {
                // reached target position (piston is fully extended)
                return false;
            }
            transform.position = newPosition;
            return true;
        }
        else
        {
            Vector3 newPosition = Vector3.MoveTowards(transform.position, _retractedPosition, _retractingSpeed * Time.deltaTime);
            if (newPosition == transform.position)
            {
                // reached target position (piston is fully retracted)
                return false;
            }
            transform.position = newPosition;
            return true;
        }
    }
}