using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Valve : PneumaticComponent
{
    public const int DefaultValveId = -1;
    public int ValveId { get { return _valveId; } private set { _valveId = value; } }
    [SerializeField]
    private int _valveId = DefaultValveId;

    public bool Closed { get; private set; } = false;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">HERE</see>
    /// </summary>
    void Start()
    {
        RoboStatus.Instance.RegisterValve(this);
    }


    /// <summary>
    /// Sets input pressure of the valve.
    /// </summary>
    /// <param name="pressure">The specified input pressure.</param>
    public override void SetInputPressure(PressureState pressure)
    {
        _inputPressure = pressure;
        _logger.CombinedLog("Input pressure of valve set to " + _inputPressure.ToString(), LogLevel.Debug);
        UpdateNextComponentInput();
    }


    /// <summary>
    /// Updates the input pressure of the next pneumatic component.
    /// </summary>
    /// <param name="pressure">The pressure (Not used in this class but in sister classes).</param>
    public override void UpdateNextComponentInput(PressureState pressure = PressureState.Undefined)
    {
        if (_nextComponent)
        {
            if (Closed)
            {
                // A closed valve connects the input with the output -> relay input pressure to next component
                _nextComponent.SetInputPressure(_inputPressure);
            }
            else
            {
                // An open valve connects the output with an air vent -> release pressure of next component
                _nextComponent.SetInputPressure(PressureState.ReleasePressure);
            }
        }
    }

    /// <summary>
    /// Sets valve state 
    /// 'closed' means that air can flow from connector 'A' to 'B'. 
    /// 'not closed' means that connector 'B' is connected to the air vent. 
    /// </summary>
    /// <param name="closed">Valve closed state.</param>
    public void SetState(bool closed)
    {
        _logger.CombinedLog("Valve closed set to " + closed, LogLevel.Info);
        Closed = closed;
        UpdateNextComponentInput();
    }
}