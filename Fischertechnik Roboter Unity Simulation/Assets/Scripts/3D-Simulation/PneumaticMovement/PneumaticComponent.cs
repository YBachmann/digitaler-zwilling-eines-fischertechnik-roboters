using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PneumaticComponent : MonoBehaviour
{
    public enum PressureState
    {
        Undefined,
        PositivePressure,
        NegativePressure,
        HoldPressure,
        ReleasePressure
    }

    [SerializeField]
    protected PneumaticComponent _nextComponent;
    protected PressureState _inputPressure = PressureState.ReleasePressure;

    protected Logger _logger = Logger.Instance;

    /// <summary>
    /// Sets input pressure of current pneumatic component.
    /// </summary>
    /// <param name="pressure">The specified input pressure.</param>
    public abstract void SetInputPressure(PressureState pressure);

    /// <summary>
    /// Updates the input pressure of the next pneumatic component.
    /// </summary>
    /// <param name="pressure">The pressure (optional).</param>
    public abstract void UpdateNextComponentInput(PressureState pressure);
}
