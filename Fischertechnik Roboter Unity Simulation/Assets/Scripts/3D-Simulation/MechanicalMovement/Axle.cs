using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Axle : Actuatable
{
    /// <summary>
    /// Do not use this function. An axle cannot move by teeth.
    /// </summary>
    /// <param name="teeth">Number of teeth to move.</param>
    public override void MoveByTeeth(float teeth)
    {
        _logger.CombinedLog("Actuatable of type 'Axle' cannot move by teeth!", LogLevel.Error);
    }

    /// <summary>
    /// Moves axle by specified number of degrees.
    /// </summary>
    /// <param name="degrees">The degrees to move.</param>
    public override void MoveByDegrees(float degrees)
    {
        if (_flipDirection)
            degrees = -degrees;

        transform.Rotate(_transformAxis * degrees);
        foreach (Actuatable child in _children)
        {
            if (child == null)
            {
                _logger.CombinedLog("Child of Axle '" + gameObject.name + "' is null! ", LogLevel.Error);
                continue;
            }
            if (child is Gear)
            {
                child.MoveByDegrees(degrees);
            }
            else if (child is Axle)
            {
                child.MoveByDegrees(degrees);
            }
            else
            {
                _logger.CombinedLog("Child '" + child + "'has invalid type '" + child.GetType() + "'", LogLevel.Error);
            }
        }

    }

}