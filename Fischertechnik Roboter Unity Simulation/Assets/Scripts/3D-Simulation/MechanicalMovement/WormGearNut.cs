using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormGearNut : Gear
{
    // circular pitch = Distance from one face of a tooth to the corresponding face of an adjacent tooth on the same gear, measured along the pitch circle.
    // module = circular pitch / pi
    // chordal pitch = Distance from one face of a tooth to the corresponding face of an adjacent tooth on the same gear, measured in a straight line. 
    // Account for scaling (all calculations are in mm but the Unity standard scale is 1 Uniyt-Unit = 1 meter) 
    private const float _defaultCordalPitchInMeters = 0.004725f; // TODO: candidate for reading from a config file
    [SerializeField]
    private float _chordalPitchInMeters = _defaultCordalPitchInMeters;

    /// <summary>
    /// Moves worm gear nut by specified number of teeth.
    /// </summary>
    /// <param name="teeth">Number of teeth to move.</param>
    public override void MoveByTeeth(float teeth)
    {
        if (_flipDirection)
            teeth = -teeth;
        float distance = teeth * _chordalPitchInMeters;
        transform.Translate(_transformAxis * distance);

        // A Worm gear nut cannot have child gears/Axles so we don't call MoveByTeeth / MoveByDegrees here
    }

    /// <summary>
    /// Moves worm gear nut by specified number of degrees. 
    /// (Not implemented, because currently not needed).
    /// </summary>
    /// <param name="degrees">The degrees to move.</param>
    public override void MoveByDegrees(float degrees)
    {
        _logger.CombinedLog("Actuatable of type 'WormGearNut' cannot move by degrees!", LogLevel.Error);
    }
}
