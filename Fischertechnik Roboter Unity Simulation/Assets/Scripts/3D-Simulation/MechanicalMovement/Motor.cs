using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Motor : MonoBehaviour
{
    [SerializeField]
    private Axle _axle;

    private const float _ticksPerRevolution = 63.9f; // TODO: possible candidate for reading from a config file
    private const int _defaultMotorId = -1;

    // There are two fields for motorID in order to keep it private but allow access from the Unity inspector 
    // https://answers.unity.com/questions/915032/make-a-public-variable-with-a-private-setter-appea.html
    public int MotorID { get { return _motorID; } private set { _motorID = value; } }
    [SerializeField]
    private int _motorID = _defaultMotorId;

    [SerializeField]
    private float _motorSpeedModificator = 0.3f;

    public float Pace { get; private set; } = 0;
    public float RemainingTicks { get; private set; } = 0;

    private Logger _logger;


    /// <summary>
    /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">HERE</see>
    /// </summary>
    void Start()
    {
        _logger = Logger.Instance;

        checkInitialParameters();

        RoboStatus.Instance.RegisterMotor(this);
    }

    /// <summary>
    /// Check initial parameters and make log entry if something is wrong.
    /// </summary>
    private void checkInitialParameters()
    {
        if (MotorID == _defaultMotorId)
        {
            _logger.CombinedLog("Motor '" + gameObject.name + "' still has its default MotorID. Please set the ID of this motor in the inspector window. ", LogLevel.Info);
        }
        if (_axle == null)
        {
            _logger.CombinedLog("Motor '" + gameObject.name + "' does not have an _axle connected to it. Please select an _axle in the inspector window. ", LogLevel.Warn);
        }
    }

    /// <summary>
    /// Frame-rate independent MonoBehaviour.FixedUpdate message for physics calculations.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.FixedUpdate.html">HERE</see>
    /// If the motor still has an action to perform it will do so here. 
    /// </summary>
    private void FixedUpdate()
    {
        // Check if there is an action to perform
        if (RemainingTicks > 0 && Pace != 0)
        {
            // calculate (absolute) amount of ticks to rotate this frame
            float ticks = Math.Abs(Pace) * Time.deltaTime * _motorSpeedModificator;

            // used for the last frame of the movement in order to hit exactly 0 remaining ticks
            if (ticks > RemainingTicks)
            {
                ticks = RemainingTicks;
            }

            // calculate rotation from ticks
            float rotation = 360 * (ticks / _ticksPerRevolution);
            if (Pace < 0)
            {
                rotation = -rotation;
            }

            // rotate connected _axle
            if (_axle != null)
            {
                _axle.MoveByDegrees(rotation);
            }

            // remove performed rotation (in ticks) from the ticks that have to be done in total 
            RemainingTicks -= ticks;

            // reset ticks and Pace if movement is completed
            if (RemainingTicks <= 0)
            {
                RemainingTicks = 0;
                Pace = 0;
            }
        }
    }

    /// <summary>
    /// Move the motor
    /// </summary>
    /// <param name="ticks">The ticks/steps that the motor shall move. Must be positive. </param>
    /// <param name="pace">The pace at which the motor shall move. Make this negative to change direction of the motor. </param>
    public void Move(int ticks, int pace)
    {
        if (ticks < 0)
        {
            _logger.CombinedLog("When moving a motor 'ticks' must be positive. If the motor shall move backwards, make pace negative. ", LogLevel.Warn);
            ticks = Math.Abs(ticks);
        }
        this.Pace = pace;
        RemainingTicks = ticks;
    }
}

