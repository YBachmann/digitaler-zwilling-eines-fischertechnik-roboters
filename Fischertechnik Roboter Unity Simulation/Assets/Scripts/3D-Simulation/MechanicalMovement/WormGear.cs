using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormGear : Gear
{

    /// <summary>
    /// Moves worm gear by specified number of teeth.
    /// </summary>
    /// <param name="teeth">Number of teeth to move.</param>
    public override void MoveByTeeth(float teeth)
    {
        if (_flipDirection)
            teeth = -teeth;

        float rotation = 360 * teeth;
        transform.Rotate(_transformAxis * rotation);

        foreach (Actuatable child in _children)
        {
            if (child is Gear)
            {
                child.MoveByTeeth(-teeth);
            }
            else if (child is Axle)
            {
                child.MoveByDegrees(rotation);
            }
            else
            {
                _logger.CombinedLog("Child '" + child + "'has invalid type '" + child.GetType() + "'", LogLevel.Error);
            }
        }
    }

    /// <summary>
    /// Moves worm gear by specified number of degrees.
    /// </summary>
    /// <param name="degrees">The degrees to move.</param>
    public override void MoveByDegrees(float degrees)
    {
        if (_flipDirection)
            degrees = -degrees;

        float teeth = degrees / 360;
        transform.Rotate(_transformAxis * degrees);

        foreach (Actuatable child in _children)
        {
            if (child is Gear)
            {
                child.MoveByTeeth(-teeth);
            }
            else if (child is Axle)
            {
                child.MoveByDegrees(degrees);
            }
            else
            {
                _logger.CombinedLog("Child '" + child + "'has invalid type '" + child.GetType() + "'", LogLevel.Error);
            }
        }
    }
}
