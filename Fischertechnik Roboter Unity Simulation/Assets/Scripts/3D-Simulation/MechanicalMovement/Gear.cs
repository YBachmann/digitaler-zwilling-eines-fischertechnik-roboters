using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Gear class is used to easily differentiate between gear- and axle-objects
/// </summary>
/// <seealso cref="Actuatable" />
public abstract class Gear : Actuatable
{
}