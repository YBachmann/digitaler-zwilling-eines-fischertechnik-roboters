using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Actuatable : MonoBehaviour
{
    [SerializeField]
    protected Actuatable[] _children = new Actuatable[] { };
    [SerializeField]
    protected bool _flipDirection = false;
    [SerializeField]
    protected Vector3 _transformAxis;

    protected Logger _logger = Logger.Instance;


    /// <summary>
    /// Moves actuable by specified number of teeth.
    /// </summary>
    /// <param name="teeth">Number of teeth to move.</param>
    public abstract void MoveByTeeth(float teeth);


    /// <summary>
    /// Moves actuable by specified number of degrees.
    /// </summary>
    /// <param name="degrees">The degrees to move.</param>
    public abstract void MoveByDegrees(float degrees);
}
