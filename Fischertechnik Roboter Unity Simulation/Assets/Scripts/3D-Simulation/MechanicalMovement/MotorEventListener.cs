using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class MotorEventListener : MonoBehaviour, IEventListener
{
    [SerializeField]
    private Motor _motor;

    private Logger _logger;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">HERE</see>
    /// </summary>
    void Start()
    {
        _logger = Logger.Instance;

        if (_motor == null)
        {
            _logger.CombinedLog("MotorEventListener '" + gameObject.name + "' does not have a reference to a motor set. This eventListener will have no effect therefore. "
                        + "Please select a motor in the inspector window. ", LogLevel.Warn);
        }

        RoboStatus.Instance.eventPublisher.subscribe(EventPublisher.EventType.MotorInstruction, this);
    }

    /// <summary>
    /// Decode json data and send instructions to motor. 
    /// </summary>
    /// <param name="jsonData">The json data.</param>
    public void UpdateListener(string jsonData)
    {
        if (_motor == null)
        {
            return;
        }

        // convert json back to list of dicts
        List<Dictionary<string, int>> MotorInstructions = JsonConvert.DeserializeObject<List<Dictionary<string, int>>>(jsonData);

        foreach (Dictionary<string, int> listElement in MotorInstructions)
        {
            try
            {
                // check if list of instructions contain one for this motor
                if (listElement["motorID"] == _motor.MotorID)
                {
                    _motor.Move(listElement["ticks"], listElement["pace"]);
                    break;
                }
            }
            catch (KeyNotFoundException e)
            {
                _logger.CombinedLog("Motor instruction contains invalid key. Exception: " + e, LogLevel.Error);
                continue;
            }
        }
    }
}
