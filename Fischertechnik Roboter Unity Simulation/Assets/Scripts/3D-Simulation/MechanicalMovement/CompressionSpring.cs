using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompressionSpring : MonoBehaviour
{
    [SerializeField]
    private GameObject _anchorGameObj;
    [SerializeField]
    private float _springConstant = 10;
    [SerializeField]
    private float _dampeningFactor = 0.1f;

    private Vector3 _restingPosition;
    private Rigidbody _rigidbody;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html">HERE</see>
    /// </summary>
    void Start()
    {
        //Fetch the Rigidbody from the GameObject with this script attached
        _rigidbody = GetComponent<Rigidbody>();

        _restingPosition = _anchorGameObj.transform.position - transform.position;
    }

    /// <summary>
    /// Frame-rate independent MonoBehaviour.FixedUpdate message for physics calculations.
    /// See Unity documentation <see href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.FixedUpdate.html">HERE</see>
    /// </summary>
    void FixedUpdate()
    {
        Vector3 deflection = (_anchorGameObj.transform.position - transform.position) - _restingPosition;

        // Calculate spring force
        Vector3 _springForce = _springConstant * deflection;

        // Calculate dampening force
        Vector3 _dampeningForce = (_rigidbody.velocity - _anchorGameObj.GetComponent<Rigidbody>().velocity) * _dampeningFactor;

        //Apply spring and dampening force
        _rigidbody.AddForce(_springForce - _dampeningForce);
    }
}
