﻿namespace Controller
{
    class ResponseStartOnline : Response
    {
        public ResponseStartOnline() : base(ftIF2013ResponseId.ftIF2013ResponseId_StartOnline)
        {
            // This Class can be extended if needed. 
        }
    }
}