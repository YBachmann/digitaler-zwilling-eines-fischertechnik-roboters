﻿using System.Collections;
using UnityEngine;

namespace Controller
{
    public class ResponseHandler
    {
        private ResponseFactory _responseFactory;        

        private int _rotationResponseId = 0;
        private int _verticalRespnseId = 0;
        private int _horizontalResponseId = 0;

        private bool _rotationTrigger = false;
        private bool _verticalTrigger = false;
        private bool _horizontalTrigger = false;

        private RoboStatus _roboStatus;

        public ResponseHandler()
        {
            _responseFactory = new ResponseFactory();
            _roboStatus = RoboStatus.Instance;
        }

        /// <summary>
        /// Create a response to a certain command ID
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public byte[] CreateResponse(byte[] command)
        {      
            // update commandIds
            if(_roboStatus.GetMotorPace(1) == 0)
            {
                _rotationResponseId = _roboStatus.GetMotorCommandId(1);
            }
            if (_roboStatus.GetMotorPace(2) == 0)
            {
                _verticalRespnseId = _roboStatus.GetMotorCommandId(2);
            }
            if (_roboStatus.GetMotorPace(3) == 0)
            {
                _horizontalResponseId = _roboStatus.GetMotorCommandId(3);
            }

            // if trigger is pressed and direction is towards trigger
            // update rotation Trigger
            if (_roboStatus.GetTriggerStatus(1) && _roboStatus.GetMotorPace(1) < 0)
                _rotationTrigger = true;
            else
                _rotationTrigger = false;
            // update vertical Trigger
            if (_roboStatus.GetTriggerStatus(2) && _roboStatus.GetMotorPace(2) < 0)
                _verticalTrigger = true;
            else
                _verticalTrigger = false;
            // update horizontal Trigger
            if (_roboStatus.GetTriggerStatus(3) && _roboStatus.GetMotorPace(3) < 0)
                _horizontalTrigger = true;
            else
                _horizontalTrigger = false;

            // create Response Object
            uint commandId = (uint)GetCommandId(command);
            Response responseObject = _responseFactory.GetResponse(commandId);

            // write commandIds to response
            if (responseObject.GetType() == typeof(ResponseExchangeData))
            {               
                responseObject.WriteTriggers(_rotationTrigger, _verticalTrigger, _horizontalTrigger); 
                responseObject.WriteMotorID(_rotationResponseId, _verticalRespnseId, _horizontalResponseId);
            }

            return responseObject.ToByteArray();
        }

        /// <summary>
        /// Get the command Id to a specific command byte array.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private long GetCommandId(byte[] command)
        {
            return TxtUtil.ByteArray2long(command);
        }
    }
}
