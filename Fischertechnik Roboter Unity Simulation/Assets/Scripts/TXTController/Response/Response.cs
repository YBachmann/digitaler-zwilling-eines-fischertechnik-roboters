﻿namespace Controller
{
    public abstract class Response
    {
        uint _messageID;
        byte[] m_id = new byte[4];

        /// <summary>
        /// Construcor sets the Response id suitable for the created Child-Class
        /// </summary>
        /// <param name="id"></param>
        public Response(ftIF2013ResponseId id)
        {
            _messageID = (uint)id;
            m_id = _messageID.ToByteArray();
        }

        /// <summary>
        /// Return the message ID as byte arry
        /// </summary>
        /// <returns></returns>
        public virtual byte[] ToByteArray()
        {
            return m_id;
        }
        public virtual void WriteTriggers(bool rotationTrigger, bool verticalTrigger, bool horizontalTrigger)
        {
            throw new System.NotImplementedException("This method can only be called on a ExchangeData object type");
        }
        public virtual void WriteMotorID(int rotationCommandId, int verticalCommandId, int horizontalCommandId)
        {
            throw new System.NotImplementedException("This method can only be called on a ExchangeData object type");
        }
        
    };
}