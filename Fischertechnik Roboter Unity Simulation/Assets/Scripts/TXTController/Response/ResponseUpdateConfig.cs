﻿namespace Controller
{
    class ResponseUpdateConfig : Response
    {
        public ResponseUpdateConfig() : base(ftIF2013ResponseId.ftIF2013ResponseId_UpdateConfig)
        {
            // This Class can be extended if needed. 
        }
    };
}