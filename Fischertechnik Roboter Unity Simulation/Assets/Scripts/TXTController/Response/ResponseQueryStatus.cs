﻿using UnityEngine;

namespace Controller
{
    class ResponseQueryStatus : Response
    {
        byte[] m_id = new byte[4];
        byte[] m_devicename = new byte[16];
        byte[] m_version = new byte[4];

        public ResponseQueryStatus() : base(ftIF2013ResponseId.ftIF2013ResponseId_QueryStatus)
        {
            m_id = base.ToByteArray();
        }

        private void WriteDeviceName()
        {
            TxtUtil.FillByteArray(ref m_devicename, Constants.Devicename.ToByteArray());
        }

        private void WriteVersion()//Todo: could be changed to be in line with Writing in ExchangeData
        {
            TxtUtil.FillByteArray(ref m_version, Constants.Version.ToByteArray());
        }

        /// <summary>
        /// Ovverrides the basic toByteArray method from the parent class "Response".
        /// Converts every data block, used in the communication protocol to one long data block.
        /// </summary>
        /// <returns></returns>
        public override byte[] ToByteArray()
        {          
            byte[] query_status_array = new byte[0];

            WriteDeviceName();
            WriteVersion();

            //m_id
            query_status_array = query_status_array.ConcatToEnd(base.ToByteArray());
            // m_devicename
            query_status_array = query_status_array.ConcatToEnd(m_devicename);
            // m_version
            query_status_array = query_status_array.ConcatToEnd(m_version);

            return query_status_array;
        }
    };
}