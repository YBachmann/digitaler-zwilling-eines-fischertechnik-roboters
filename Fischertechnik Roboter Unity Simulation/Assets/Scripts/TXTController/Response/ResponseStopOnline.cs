﻿namespace Controller
{
    class ResponseStopOnline : Response
    {
        public ResponseStopOnline() : base(ftIF2013ResponseId.ftIF2013ResponseId_StopOnline)
        {
            // This Class can be extended if needed. 
        }
    }
}