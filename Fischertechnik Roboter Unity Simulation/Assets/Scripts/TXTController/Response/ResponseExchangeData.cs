﻿using System;
using System.Linq;
using static TxtUtil;

namespace Controller
{
    class ResponseExchangeData : Response
    {   
        byte[] m_universalInputs = new byte[16]; // byte [0,1] rot_trigger. [2,3] vert_trigger. [4,5] hori_trigger
        byte[] m_counter_input = new byte[8];
        byte[] m_counter_value = new byte[8];
        byte[] m_counter_command_id = new byte[8];
        byte[] m_moter_command_id = new byte[8]; // byte [0,1]: rot_motor. [2,3] hori_motor
        byte[] m_sound_command_id = new byte[2];
        byte[][] m_ir = new byte[5][]; // Will contain 5 IR byte arrays

        byte[] IR = new byte[5]; // 1: leftX, 2: leftY, 3: rightX, 4: rightY, 5:bits

        public ResponseExchangeData() : base(ftIF2013ResponseId.ftIF2013ResponseId_ExchangeData)
        {
            for (int i = 0; i < m_ir.Length; i++)
            {
                m_ir[i] = IR;
            }
        }

        /// <summary>
        /// Special Method to write all Sensor data to the assigned data blocks.
        /// </summary>
        public override void WriteTriggers(bool rotationTrigger, bool verticalTrigger, bool horizontalTrigger)
        {
            // write rotation sensor                        
            TxtUtil.CopyTo(m_universalInputs, rotationTrigger.ToByteArray(), 0);
            
            //write vertical sensor            
            TxtUtil.CopyTo(m_universalInputs, verticalTrigger.ToByteArray(), 2);
            
            // write horizontal sensor          
            TxtUtil.CopyTo(m_universalInputs, horizontalTrigger.ToByteArray(), 4);
        }

        /// <summary>
        /// Special method to write all the motor ids in their associated data blocks.
        /// </summary>
        public override void WriteMotorID(int rotationCommandId, int verticalCommandId, int horizontalCommandId)
        {
            // rotation Motor id            
            TxtUtil.CopyTo(m_moter_command_id, rotationCommandId.ToByteArray(), 0);

            // vertical Motor id                 
            TxtUtil.CopyTo(m_moter_command_id, verticalCommandId.ToByteArray(), 2);

            // horizontal motor id                 
            TxtUtil.CopyTo(m_moter_command_id, horizontalCommandId.ToByteArray(), 4);
        }        

        /// <summary>
        /// Ovverrides the basic toByteArray method from the parent class "Response".
        /// Converts every data block, used in the communication protocol to one long data block.
        /// </summary>
        /// <returns></returns>
        public override byte[] ToByteArray()
        {
            byte[] exchange_data_array = new byte[0];           

            //m_id
            exchange_data_array = exchange_data_array.ConcatToEnd(base.ToByteArray());
            // Universal Inputs (trigger status)
            exchange_data_array = exchange_data_array.ConcatToEnd(m_universalInputs);
            // Counter input
            exchange_data_array = exchange_data_array.ConcatToEnd(m_counter_input);
            // Counter value
            exchange_data_array = exchange_data_array.ConcatToEnd(m_counter_value);
            // Counter command id
            exchange_data_array = exchange_data_array.ConcatToEnd(m_counter_command_id);
            // Motor command id (indicate if motor is done)
            exchange_data_array = exchange_data_array.ConcatToEnd(m_moter_command_id);
            // Sound command id
            exchange_data_array = exchange_data_array.ConcatToEnd(m_sound_command_id);

            // Infrared Signals
            for (int i = 0; i < m_ir.Length; i++)
            {
                exchange_data_array = exchange_data_array.ConcatToEnd(m_ir[i]);
            }

            return exchange_data_array;
        }
    }
}