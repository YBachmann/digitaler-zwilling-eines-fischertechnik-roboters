namespace Controller
{
	public enum ftIF2013ResponseId : uint
	{
		ftIF2013ResponseId_QueryStatus = 0xBAC9723E,
		ftIF2013ResponseId_StartOnline = 0xCA689F75,
		ftIF2013ResponseId_UpdateConfig = 0x9689A68C,
		ftIF2013ResponseId_ExchangeData = 0x4EEFAC41,
		ftIF2013ResponseId_ExchangeDataCmpr = 0x6F3B54E6,
		ftIF2013ResponseId_StopOnline = 0xFBF600D2,
		ftIF2013ResponseId_StartCameraOnline = 0xCF41B24E,
		ftIF2013ResponseId_StopCameraOnline = 0x4B3C1EB6,
		// Used in camera channel
		ftIF2013DataId_CameraOnlineFrame = 0xBDC2D7A1		
	};

	static class Constants
	{
		public const string Devicename = "TX2013";
		public const uint Version = 0x04020400;
	}

	public class ResponseFactory
	{
		public Response GetResponse(uint commandId)
		{
			switch (commandId)
			{
				case (uint)FtIF2013CommandId.ftIF2013CommandId_QueryStatus:
					return new ResponseQueryStatus();

				case (uint)FtIF2013CommandId.ftIF2013CommandId_StartOnline:
					return new ResponseStartOnline();

				case (uint)FtIF2013CommandId.ftIF2013CommandId_UpdateConfig:
					return new ResponseUpdateConfig();

				case (uint)FtIF2013CommandId.ftIF2013CommandId_ExchangeData:
					return new ResponseExchangeData();

				case (uint)FtIF2013CommandId.ftIF2013CommandId_ExchangeDataCmpr:
					throw new System.NotImplementedException("The functionality for the feature \"ExchangeDataCmpr\" has not been implemented yet.");					

				case (uint)FtIF2013CommandId.ftIF2013CommandId_StopOnline:
					return new ResponseStopOnline();

				case (uint)FtIF2013CommandId.ftIF2013CommandId_StartCameraOnline:
					throw new System.NotImplementedException("The functionality for the feature \"StartCameraOnline\" has not been implemented yet.");

				case (uint)FtIF2013CommandId.ftIF2013CommandId_StopCameraOnline:
					throw new System.NotImplementedException("The functionality for the feature \"StopCameraOnline\" has not been implemented yet.");

				case (uint)FtIF2013CommandId.ftIF2013AcknowledgeId_CameraOnlineFrame:
					throw new System.NotImplementedException("The functionality for the feature \"CameraOnlineFrame\" has not been implemented yet.");			

				default:
					throw new System.Exception("Unknown ResponseId. Check if your ID is missing in ftIF2013ResponseId");
			}

		}
	}
}