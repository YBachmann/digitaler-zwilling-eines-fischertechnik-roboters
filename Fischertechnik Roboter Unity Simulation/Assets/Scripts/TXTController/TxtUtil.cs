using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// This class contains methods needed by the TXT Controller.
/// Firs up are the methods used to convert a Byte-Array to something else.
/// Then there are methods to turn somethin into a Byte-Array.
/// Last we have the methods to handle byte-arrays.
/// </summary>
public static class TxtUtil
{
    //------byte Array to someting------//
    /// <summary>
    /// Represent the a byte array as string. Does not interpret values but rather just displays the bytes in a row.
    /// Should only be used to Debug communication.
    /// </summary>
    /// <param name="byteArry"></param>
    /// <returns></returns>
    public static string ByteArrayAsString(byte[] byteArry)
    {
        string output = "";
        foreach (byte element in byteArry)
        {
            output = output + element + ", ";
        }
        return output;
    }

    /// <summary>
    /// Turn a byte array of length four into a long value.
    /// </summary>
    /// <param name="byte_array"></param>
    /// <returns></returns>
    public static long ByteArray2long(byte[] byte_array)
    {
        long number;
        //long id = BitConverter.ToInt64(byte_array_id.Take(4).ToArray(), startIndex: 0);
        number = ((int)byte_array[0]) & 0x000000ff;
        number += (((int)byte_array[1]) << 8) & 0x0000ff00;
        number += (((int)byte_array[2]) << 16) & 0x00ff0000;
        number += (((int)byte_array[3]) << 24) & 0xff000000;
        return number;
    }

    /// <summary>
    /// Turn a Byte-Array of length 2 into an Integer
    /// </summary>
    /// <param name="byte_array"></param>
    /// <returns></returns>
    public static int ByteArray2int(byte[] byte_array)
    {
        int number;
        //long id = BitConverter.ToInt64(byte_array_id.Take(4).ToArray(), startIndex: 0);
        number = ((int)byte_array[0]) & 0x00ff;
        number += (((int)byte_array[1]) << 8) & 0xff00;

        return number;
    }

    /// <summary>
    /// Extension method. Can be used on variables of the datatype byte[].
    /// Convert two selected bytes in a byte array to an integer
    /// </summary>
    /// <param name="byte_array"></param>
    /// <param name="skip"></param>
    /// <returns></returns>
    public static int ToInt(this byte[] byte_array, int skip)
    {
        return ByteArray2int(byte_array.TakeSplit(skip, 2));
    }    


    //------Something to byte Array------//
    /// <summary>
    /// Turn a long value into a byte array of lenght 4
    /// </summary>
    /// <param name="i"></param>
    /// <returns></returns>
    public static byte[] Long2ByteArray(long i)
    {
        byte[] byte_array = new byte[4];
        byte_array[0] = (byte)i;
        byte_array[1] = (byte)(i >> 8);
        byte_array[2] = (byte)(i >> 16);
        byte_array[3] = (byte)(i >> 24);
        return byte_array;
    }

    /// <summary>
    /// Turn an integer into a Byte-array of lenth 2
    /// </summary>
    /// <param name="i"></param>
    /// <returns></returns>
    public static byte[] Int2ByteArray(int i)
    {
        byte[] byte_array = new byte[2];
        byte_array[0] = (byte)i;
        byte_array[1] = (byte)(i >> 8);

        return byte_array;
    }

    /// <summary>
    /// Extension method. Can be used on variables of the datatype bool.
    /// Turn a boolean into a byte array of size 2
    /// True is represented by one, 0 is represented by 0.
    /// </summary>
    /// <param name="boolean"></param>
    /// <returns></returns>
    public static byte[] ToByteArray(this bool boolean)
    {
        if (boolean)
        {
            return Int2ByteArray(1);
        }
        else
        {
            return Int2ByteArray(0);
        }
    }

    /// <summary>
    /// Extension method. Can be used on variables of the datatype int.
    /// Turns an integer into a byte array of length 2. 
    /// </summary>
    /// <param name="integer"></param>
    /// <returns></returns>
    public static byte[] ToByteArray(this int integer)
    {
        return Int2ByteArray(integer);
    }

    /// <summary>
    /// Extension method. Can be used on variables of the datatype string.
    /// Turns a string into a byte array. The lenght of the byte array depends on the lenth of the used string.
    /// </summary>
    /// <param name="text"></param>
    /// <returns>byte array</returns>
    public static byte[] ToByteArray(this string text)
    {
        byte[] output = Encoding.ASCII.GetBytes(text);
        return output;
    }

    /// <summary>
    /// Extension method. Can be used on variables of the datatype uint.
    /// Turns an unsigned integer into a byte array of size 4
    /// </summary>
    /// <param name="integer"></param>
    /// <returns>byte array</returns>
    public static byte[] ToByteArray(this uint integer)
    {
        byte[] output = new byte[4];
        output[0] = (byte)integer;
        output[1] = (byte)(integer >> 8);
        output[2] = (byte)(integer >> 16);
        output[3] = (byte)(integer >> 24);
        return output;
    }

    
    //------Handle byte Arrays------//
    /// <summary>
    /// Extension method. Can be used on variables of the datatype byte[].
    /// Concatenate the source byte array to the end of the destination (this) byte array.
    /// </summary>
    /// <param name="destination">the byte array on which end the source array will be concatenated</param>
    /// <param name="source">the byte array which will be concatenated to the end</param>
    /// <returns>The destination- plus source array</returns>
    public static byte[] ConcatToEnd(this byte[] destination, byte[] source)
    {
        byte[] byteArray = new byte[destination.Length + source.Length];
        if (destination == null)
        {
            byteArray = source;
        }
        // Todo: Exception if source array is 
        byteArray = destination.Concat(source).ToArray();

        return byteArray;
    }

    /// <summary>
    /// Extension method. Can be used on variables of the datatype byte[].
    /// Copy one byte array to another at a certain index position
    /// </summary>
    /// <param name="destination"></param>
    /// <param name="source"></param>
    /// <param name="index"></param>
    public static void CopyTo(this byte[] destination, byte[] source, int index)
    {
        for (int i = 0; i < source.Length; i++)
        {
            destination[i + index] = source[i];
        }
    }

    /// <summary>
    /// Fill a byte array with the values from another.
    /// </summary>
    /// <param name="toBeFilled"></param>
    /// <param name="filler"></param>
    public static void FillByteArray(ref byte[] toBeFilled, byte[] filler)
    {
        if(toBeFilled.Length < filler.Length)
        {
            throw new System.Exception("The Array you are trying to fill is shorter than the array you want to fill it with");
        }
        else
        {
            for (int i = 0; i < filler.Length; i++)
            {
                toBeFilled[i] = filler[i];
            }
        }
        
    }

    /// <summary>
    /// Extension method. Can be used on variables of the datatype byte[].
    /// return a split of a byte array
    /// </summary>
    /// <param name="source"></param>
    /// <param name="skip"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    public static byte[] TakeSplit(this byte[]source, int skip, int length)
    {
        return source.Skip(skip).Take(length).ToArray();
    }
}
