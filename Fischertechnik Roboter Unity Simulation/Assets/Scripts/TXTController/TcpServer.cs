using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using Controller;
public class TcpServer
{
    private TcpListener _tcpListener;
    private Thread _tcpListenerThread;
    private TcpClient _connectedTcpClient;

	private TxtController _controller;
	private Logger _logger;

	private string _localIp = "127.0.0.1";
    private int _localPort = 65000;

	/// <summary>
	/// Construcor starting a TCP Server Socket
	/// </summary>
	/// <param name="localPort"></param>
	/// <param name="controller"></param>
	public TcpServer(int localPort, TxtController controller)
    {
		_localIp = IPAddress.Any.ToString();		
		this._localPort = localPort;
		this._controller = controller;		
		_logger = Logger.Instance;

		// Start TcpServer background thread 		
		_tcpListenerThread = new Thread(new ThreadStart(ListenForIncommingRequests));
		_tcpListenerThread.IsBackground = true;
        
		_tcpListenerThread.Start();
		
	}

	/// <summary>
	/// Return all IP-Network adresses that dont start with 172
	/// </summary>
	/// <returns></returns>
	public string GetLocalIPAddress()
	{
		var host = Dns.GetHostEntry(Dns.GetHostName());
		string output = "";
		foreach (var ip in host.AddressList)
		{
			if (ip.AddressFamily == AddressFamily.InterNetwork)
			{	
				if(!ip.ToString().StartsWith("172"))
					output = output + ip.ToString() + "\n";
			}
		}
		if (output != "")
        {
			return output;
        }
		throw new Exception("No network adapters with an IPv4 address in the system!");
	}

	/// <summary>
	/// Listens for incoming connections and reads incoming data.
	/// After a connection is established, this method will wait for incoming data.
	/// HandleInteraction is then called with the received Data.
	/// TODO: Check if this function is really running in its own Thread...
	/// </summary>
	private void ListenForIncommingRequests()
	{
		try
		{
			// Create listener on localhost port 8052.
			_tcpListener = new TcpListener(IPAddress.Parse(_localIp), _localPort);
			_tcpListener.Start();
			Debug.Log("Server is listening");
			_logger.CombinedLog("Server is listening", LogLevel.Debug);
			Byte[] bytes = new Byte[1024];
			while (true)
			{
				using (_connectedTcpClient = _tcpListener.AcceptTcpClient())
				{
					// Get a stream object for reading
					using (NetworkStream stream = _connectedTcpClient.GetStream())
					{
						int length;
						try
						{
							// Read incomming stream into byte arrary. 						
							while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
							{
								var incommingData = new byte[length];
								Array.Copy(bytes, 0, incommingData, 0, length);

								// Give Data to TXT-Controller							
								byte[] response = _controller.HandleInteraction(incommingData);
								SendMessage(response);					
							}
						}
						catch (Exception e)
						{
							Debug.LogWarning(e.Message);
							_controller.RestartServer();
                        }
					}
				}
			}
		}
		catch (SocketException socketException)
		{
			Debug.Log("SocketException " + socketException.ToString());			
		}
	}

	/// <summary>
	/// Send a byte array to the connected client.
	/// </summary>
	/// <param name="response">the message as byte array</param>
	public void SendMessage(byte[] response)
	{
		if (_connectedTcpClient == null)
		{
			return;
		}

		try
		{
			// Get a stream object for writing. 			
			NetworkStream stream = _connectedTcpClient.GetStream();
			if (stream.CanWrite)
			{
				// Write byte array to socketConnection stream.               
				stream.Write(response, 0, response.Length);
			}
		}
		catch (SocketException socketException)
		{
			Debug.Log("Socket exception: " + socketException);			
		}
	}

	/// <summary>
	/// Shut down the server gracefully.
	/// </summary>
	public void ShutDown()
	{
		try
		{
			_connectedTcpClient.Close();
		}
		catch (Exception e)
		{
			Debug.Log(e.Message);
		}

		// You must close the tcp listener
		try
		{
			_tcpListener.Stop();
		}
		catch (Exception e)
		{
			Debug.Log(e.Message);
		}

        try
        {
			_tcpListenerThread.Abort();
		}
		catch (Exception e)
		{
			Debug.Log(e.Message);
		}
	}
}
