﻿using Controller;

namespace Controller
{
    public enum FtIF2013CommandId : uint
    {
        ftIF2013CommandId_QueryStatus = 0xDC21219A,
        ftIF2013CommandId_StartOnline = 0x163FF61D,
        ftIF2013CommandId_UpdateConfig = 0x060EF27E,
        ftIF2013CommandId_ExchangeData = 0xCC3597BA,
        ftIF2013CommandId_ExchangeDataCmpr = 0xFBC56F98,
        ftIF2013CommandId_StopOnline = 0x9BE5082C,
        ftIF2013CommandId_StartCameraOnline = 0x882A40A6,
        ftIF2013CommandId_StopCameraOnline = 0x17C31F2F,
        // Used in camera channel
        ftIF2013AcknowledgeId_CameraOnlineFrame = 0xADA09FBA
    }

    public class CommandFactory
    {
        private Logger _logger = Logger.Instance;

        public Command getCommand(uint command_type)        {
            
            switch (command_type)
            {
                case (uint)FtIF2013CommandId.ftIF2013CommandId_QueryStatus:                    
                    return new CommandQueryStatus();

                case (uint)FtIF2013CommandId.ftIF2013CommandId_StartOnline:                    
                    return new CommandStartOnline();

                case (uint)FtIF2013CommandId.ftIF2013CommandId_UpdateConfig:                    
                    return new CommandUpdateConfig();

                case (uint)FtIF2013CommandId.ftIF2013CommandId_ExchangeData:                    
                    return new CommandExchangeData();

                case (uint)FtIF2013CommandId.ftIF2013CommandId_ExchangeDataCmpr:                    
                    throw new System.NotImplementedException("The functionality for the feature \"ExchangeDataCmpr\" has not been implemented yet.");

                case (uint)FtIF2013CommandId.ftIF2013CommandId_StopOnline:
                    return new CommandStopOnline();

                case (uint)FtIF2013CommandId.ftIF2013CommandId_StartCameraOnline:
                    throw new System.NotImplementedException("The functionality for the feature \"StartCameraOnline\" has not been implemented yet.");

                case (uint)FtIF2013CommandId.ftIF2013CommandId_StopCameraOnline:
                    throw new System.NotImplementedException("The functionality for the feature \"StopCameraOnline\" has not been implemented yet.");

                case (uint)FtIF2013CommandId.ftIF2013AcknowledgeId_CameraOnlineFrame:
                    throw new System.NotImplementedException("The functionality for the feature \"CameraOnlineFrame\" has not been implemented yet.");

                default:
                    throw new System.Exception("Unknown CommandId. Check if the ID is missing in FtIF2013CommandId");
            }
        }
    }
}
