﻿using System.Collections;
using UnityEngine;

namespace Controller
{
    public abstract class Command
    {
        /// <summary>
        /// This method will be overriden by Object that actually receive any special data combined with their id.
        /// </summary>
        /// <param name="command"></param>
        public virtual void ByteArray2Object(byte[] command)
        {
            //Debug.Log("This command doesnt requre a special Method");
        }

        // Methods should only be used by versions of the Command class that handle certain data.
        public virtual int GetRotationPace() { throw new System.NotImplementedException(); }
        public virtual int GetVerticalPace() { throw new System.NotImplementedException(); }
        public virtual int GetHorizontalPace() { throw new System.NotImplementedException(); }

        public virtual int GetRotationTicks() { throw new System.NotImplementedException(); }
        public virtual int GetVerticalTicks() { throw new System.NotImplementedException(); }
        public virtual int GetHorizontalTicks() { throw new System.NotImplementedException(); }

        public virtual int GetRotationCommandId() { throw new System.NotImplementedException(); }
        public virtual int GetVerticalCommandId() { throw new System.NotImplementedException(); }
        public virtual int GetHorizontalCommandId() { throw new System.NotImplementedException(); }

        public virtual bool GetVacuumPumpState() { throw new System.NotImplementedException(); }
        public virtual bool GetValveState() { throw new System.NotImplementedException(); }
    };

    
}