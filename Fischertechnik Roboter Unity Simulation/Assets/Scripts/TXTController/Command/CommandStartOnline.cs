﻿using UnityEngine;
using System.Linq;

namespace Controller
{
    class CommandStartOnline : Command
    {
        // Todo: Ask professor if this is really needed.
        // The real, physical Robot does not care if the Content off this message is really "Online" Thus this method is not used.
        public bool checkMessageName(byte[] message)
        {
            string expected_message = "Online";
            // Get m_name Bytes and convert them to String
            byte[] m_name = message.Skip(4).Take(64).ToArray();
            string name = System.Text.Encoding.Default.GetString(m_name);
            // Somehow this if statement does not recognize the String "Online". Since this isnt needed its not properly implemented yet.
            if (name.Equals(expected_message))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}