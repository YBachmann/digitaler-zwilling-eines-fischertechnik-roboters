﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;

namespace Controller
{
    public class CommandHandler
    {
        private CommandFactory _commandFactory;

        private int _rotationPace = 0;
        private int _verticalPace = 0;
        private int _horizontalPace = 0;

        private bool _vacPumpState = false;
        private bool _vacValveState = false;


        private RoboStatus _roboStatus;
        private Logger _logger;

        public CommandHandler()
        {
            _commandFactory = new CommandFactory();
            _roboStatus = RoboStatus.Instance;
            _logger = Logger.Instance;

        }

        /// <summary>
        /// Special Method to handle incoming commands. 
        /// This method uses the command Factory to create the specific method, needed to handle each command.
        /// If the command data requires special treatment it will be handled here.
        /// </summary>
        /// <param name="command"></param>
        public void HandleCommand(byte[] command)
        {
            uint commandId = (uint)GetCommandId(command);
            Command commandObject = _commandFactory.getCommand(commandId);

            _logger.CombinedLog("Received " + commandObject.GetType().Name, LogLevel.Debug);

            commandObject.ByteArray2Object(command);
            if (commandObject.GetType() == typeof(CommandStartOnline))
                _roboStatus.SetOnline(true);
            // If the command is of type Exchange Data
            if (commandObject.GetType() == typeof(CommandExchangeData))
            {

                // Todo: Update Valve and Vacuum status 
                bool vacPumpState = commandObject.GetVacuumPumpState();
                if (_vacPumpState != vacPumpState)
                {
                    _vacPumpState = vacPumpState;
                    _roboStatus.UpdateVacPumpState(vacPumpState, 1);
                }

                bool vacValveState = commandObject.GetValveState();
                if (_vacValveState != vacValveState)
                {
                    _vacValveState = vacValveState;
                   _roboStatus.UpdateValveState(vacValveState, 1);
                }


                // Update motor Instruction
                List<Dictionary<string, int>> motorInstructions = new List<Dictionary<string, int>>();                
                if (_rotationPace != commandObject.GetRotationPace() ||
                _roboStatus.GetMotorCommandId(1) != commandObject.GetRotationCommandId())
                {
                    // Update rotation Instruction
                    motorInstructions.Add(new Dictionary<string, int>(){
                        {"motorID", 1},
                        {"commandID", commandObject.GetRotationCommandId() },
                        {"pace", commandObject.GetRotationPace() },
                        {"ticks", commandObject.GetRotationTicks() } // Update ticks only if there is a new command ID
                    });
                    _rotationPace = commandObject.GetRotationPace();
                }

                if (_verticalPace != commandObject.GetVerticalPace() ||
                    _roboStatus.GetMotorCommandId(2) != commandObject.GetVerticalCommandId())
                {
                    // Update vertical Instruction
                    motorInstructions.Add(new Dictionary<string, int>(){
                        {"motorID", 2},
                        {"commandID", commandObject.GetVerticalCommandId() },
                        {"pace", commandObject.GetVerticalPace() },
                        {"ticks", commandObject.GetVerticalTicks()} // Update ticks only if there is a new command ID
                    });
                    _verticalPace = commandObject.GetVerticalPace();
                }

                if (_horizontalPace != commandObject.GetHorizontalPace() ||
                    _roboStatus.GetMotorCommandId(3) != commandObject.GetHorizontalCommandId())
                {
                    // Update horizontal Instruction
                    motorInstructions.Add(new Dictionary<string, int>(){
                        {"motorID", 3},
                        {"commandID", commandObject.GetHorizontalCommandId() },
                        {"pace", commandObject.GetHorizontalPace() },
                        {"ticks", commandObject.GetHorizontalTicks()} // Update ticks only if there is a new command ID
                    });                    
                    _horizontalPace = commandObject.GetHorizontalPace();
                }
                _roboStatus.UpdateMotorInstruction(motorInstructions);             
            }
        }

        /// <summary>
        /// Get the command Id to a specific command byte array.
        /// Todo: Should be in TXT util
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private long GetCommandId(byte[] command)
        {
            return TxtUtil.ByteArray2long(command);
        }
    }
}