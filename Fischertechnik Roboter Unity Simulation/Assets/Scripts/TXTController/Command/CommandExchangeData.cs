﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Controller
{    
    class CommandExchangeData : Command
    {    
        // Message structure as byte arrays
        private byte[] m_pwmOutputValues = new byte[16];
        private byte[] m_motor_master = new byte[8];
        private byte[] m_motor_distance = new byte[8];
        private byte[] m_motor_command_id = new byte[8];
        private byte[] m_counter_reset_command_id = new byte[8];
        private byte[] sound = new byte[6];

        // Message as usable values
        // Motor pace
        private int _rotationPace = 0;
        private int _verticalPace = 0;
        private int _horizontalPace = 0;
        // Vacuum pump and valve
        private bool _vacuumPump = false;
        private bool _valve = false;
        // Motor distance
        private int _rotationTicks = 0;
        private int _verticalTicks = 0;
        private int _horizontalTicks = 0;
        // Motor command ID
        private int _rotationCommandID = 0;
        private int _verticalCommandID = 0;
        private int _horizontalCommandID = 0;

        /// <summary>
        /// Converts PwmOutputValues byte array to readable/usable values.
        /// </summary>
        private void ReadPwmOutputValues()
        {
            // read Rotation Pace
            int rotationPace = -1 * TxtUtil.ByteArray2int(m_pwmOutputValues);
            if(rotationPace == 0)
            {
                rotationPace = m_pwmOutputValues.ToInt(skip: 2);
            }
            _rotationPace = rotationPace;

            // read Vertical Pace
            int verticalPace = -1 * m_pwmOutputValues.ToInt(skip: 4);
            if( verticalPace == 0)
            {
                verticalPace = m_pwmOutputValues.ToInt(skip: 6);
            }
            _verticalPace = verticalPace;

            // read Horizontal Pace
            int horizontalPace = -1 * m_pwmOutputValues.ToInt(skip: 8);
            if (horizontalPace == 0)
            {
                horizontalPace = m_pwmOutputValues.ToInt(skip: 10);
            }
            _horizontalPace = horizontalPace;

            // read Vacuum Pump state
            int vacuumPump = m_pwmOutputValues.ToInt(skip: 12);
            if (vacuumPump == 512)
                _vacuumPump = true;            
            else
                _vacuumPump = false;

            // read Valve state
            int valve = m_pwmOutputValues.ToInt(skip: 14);
            if (valve == 512)
                _valve = true;
            else
                _valve = false;
        }

        /// <summary>
        /// Converts MotorDistance byte array to usable/readable values.
        /// </summary>
        private void readMotorDistance()
        {
            // read rotation distance 
            _rotationTicks = m_motor_distance.ToInt(skip: 0);
            if (_rotationTicks == 0)
                _rotationTicks = 2000;
            // read vertical distance 
            _verticalTicks = m_motor_distance.ToInt(skip: 2);
            if (_verticalTicks == 0)
                _verticalTicks = 2000;
            // read horizontal distance 
            _horizontalTicks = m_motor_distance.ToInt(skip: 4);
            if (_horizontalTicks == 0)
                _horizontalTicks = 2000;
        }

        /// <summary>
        /// Convert MotorCommandId byte array to usable/readable values.
        /// </summary>
        private void readMotorCommandId()
        {
            _rotationCommandID = m_motor_command_id.ToInt(skip: 0);
            _verticalCommandID = m_motor_command_id.ToInt(skip: 2);
            _horizontalCommandID = m_motor_command_id.ToInt(skip: 4);
        }

        /// <summary>
        /// Split the incoming byte array to parts, specified in the communication protocol. 
        /// Protocol is derived form the C-Programm used to adress the FTRobot via Laptop/PC code-execution
        /// </summary>
        /// <param name="command"></param>
        public override void ByteArray2Object(byte[] command)
        {
            m_pwmOutputValues = command.TakeSplit(4, 16);
            m_motor_master = command.TakeSplit(20, 8);
            m_motor_distance = command.TakeSplit(28, 8);
            m_motor_command_id = command.TakeSplit(36, 8);
            m_counter_reset_command_id = command.TakeSplit(44, 8);
            sound = command.TakeSplit(52, 6);

            // Custom methods to turn message-parts into usable values
            ReadPwmOutputValues();
            readMotorDistance();
            readMotorCommandId();
        }

        public override int GetRotationPace() { return _rotationPace; }
        public override int GetVerticalPace() { return _verticalPace; }
        public override int GetHorizontalPace() { return _horizontalPace; }

        public override int GetRotationTicks() { return _rotationTicks; }
        public override int GetVerticalTicks() { return _verticalTicks; }
        public override int GetHorizontalTicks() { return _horizontalTicks; }

        public override int GetRotationCommandId() { return _rotationCommandID; }
        public override int GetVerticalCommandId() { return _verticalCommandID; }
        public override int GetHorizontalCommandId() { return _horizontalCommandID; }

        public override bool GetVacuumPumpState() { return _vacuumPump; }
        public override bool GetValveState() { return _valve; }
    }
}
