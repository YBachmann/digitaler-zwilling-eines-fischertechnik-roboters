using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Collections.Concurrent;

namespace Controller
{
    public class TxtController : MonoBehaviour
    {
        private TcpServer _server;

        private CommandHandler _commandHandler;
        private ResponseHandler _responseHandler;

        private Logger _logger = Logger.Instance;

        private bool _endingSoftware = false;

        [SerializeField]
        private TextMeshProUGUI _ipText;

        /// <summary>
        /// Start is called before the first frame update.
        /// Initialize objects.
        /// </summary>
        void Start()
        {
            _server = new TcpServer(localPort: 65000, this);

            _commandHandler = new CommandHandler();
            _responseHandler = new ResponseHandler();

            // Can be used to get the Server IP address
            _ipText.text = _server.GetLocalIPAddress();
        }

        /// <summary>
        /// Takes a byte array and distribute it to specific handlers.
        /// </summary>
        /// <param name="command">Byte array containing incoming command</param>
        /// <returns>Byte array containing response</returns>
        public byte[] HandleInteraction(byte[] command)
        {
            _commandHandler.HandleCommand(command);
            byte[] response = _responseHandler.CreateResponse(command);

            return response;
        }

        /// <summary>
        /// Gets called when the Application is closed.
        /// </summary>
        public void OnApplicationQuit()
        {
            _endingSoftware = true;
            //_server.SendMessage(TxtUtil.Long2ByteArray((long)ftIF2013ResponseId.ftIF2013ResponseId_StartOnline));
            _server.ShutDown();            
        }

        /// <summary>
        /// Used to restart the server. 
        /// Needed if the client for example closes the connection unexpectedly
        /// </summary>
        public void RestartServer()
        {            
            RoboStatus.Instance.SetOnline(false);
            if (!_endingSoftware)
            {
                _server.ShutDown();
                _server = new TcpServer(localPort: 65000, this);
            }
        }

        public void ShutDownCommunication()
        {
            _server.SendMessage(TxtUtil.Long2ByteArray((long)ftIF2013ResponseId.ftIF2013ResponseId_StopOnline));
            RestartServer();
        }

    }
}
