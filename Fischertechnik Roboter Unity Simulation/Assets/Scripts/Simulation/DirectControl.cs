using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class DirectControl : MonoBehaviour
{
    [SerializeField]
    private Button inwards;
    [SerializeField]
    private Button outwards;
    [SerializeField]
    private Button up;
    [SerializeField]
    private Button down;
    [SerializeField]
    private Button left;
    [SerializeField]
    private Button right;
    [SerializeField]
    private Toggle pump;
    [SerializeField]
    private Toggle valve;

    [SerializeField]
    private KeyCode _hInKey;
    [SerializeField]
    private KeyCode _hOutKey;
    [SerializeField]
    private KeyCode _vDownKey;
    [SerializeField]
    private KeyCode _vUpKey;
    [SerializeField]
    private KeyCode _rLeftKey;
    [SerializeField]
    private KeyCode _rRightKey;
    [SerializeField]
    private KeyCode _pumpKey;
    [SerializeField]
    private KeyCode _valveKey;


    [SerializeField]
    private Simulation _simulation;
    [SerializeField]
    private int _pace = 400;
    private RoboStatus _roboStatus;

    private int _rotationPace = 0;    
    private bool _left = true;
    private int _verticalPace = 0;    
    private bool _down = true;
    private int _horizontalPace = 0;
    private bool _outward = true;

    // Used so the Online status doesnt update every frame
    private bool _online = false;

    private void Start()
    {
        _roboStatus = RoboStatus.Instance;

        // Update Shortcuts
        TextMeshProUGUI shortcut = inwards.transform.Find("Shortcut").GetComponent<TextMeshProUGUI>();
        shortcut.text = _hInKey.ToString();

        shortcut = outwards.transform.Find("Shortcut").GetComponent<TextMeshProUGUI>();
        shortcut.text = _hOutKey.ToString();

        shortcut = down.transform.Find("Shortcut").GetComponent<TextMeshProUGUI>();
        shortcut.text = _vDownKey.ToString();

        shortcut = up.transform.Find("Shortcut").GetComponent<TextMeshProUGUI>();
        shortcut.text = _vUpKey.ToString();

        shortcut = left.transform.Find("Shortcut").GetComponent<TextMeshProUGUI>();
        shortcut.text = _rLeftKey.ToString();

        shortcut = right.transform.Find("Shortcut").GetComponent<TextMeshProUGUI>();
        shortcut.text = _rRightKey.ToString();

        shortcut = pump.transform.Find("Shortcut").GetComponent<TextMeshProUGUI>();
        shortcut.text = _pumpKey.ToString();

        shortcut = valve.transform.Find("Shortcut").GetComponent<TextMeshProUGUI>();
        shortcut.text = _valveKey.ToString();

    }

    void Update()
    {
        // Turn off pumps if online mode starts
        if (_roboStatus.GetOnlineStatus() && !_online)
        {
            _online = true;
            pump.isOn = false;
            
            if(_roboStatus.GetVacPumpState(1))
                _simulation.ForceUpdatePump(false);
            
            if(_roboStatus.IsValveStateClosed(1))
                _simulation.ForceUpdateValve(false);
        }
        else if (!_roboStatus.GetOnlineStatus() && _online)
        {
            _online = false;
        }

        // Pointer event is used to simulate button press. 
        // The user will get visual fedback from the pressed buttons
        var pointer = new PointerEventData(EventSystem.current);
        // Handle Shortcuts

        // Rotate right
        if (Input.GetKeyDown(_rRightKey))
        {            
            ExecuteEvents.Execute(right.gameObject, pointer, ExecuteEvents.pointerDownHandler);
        }
        if (Input.GetKeyUp(_rRightKey))
        {
            ExecuteEvents.Execute(right.gameObject, pointer, ExecuteEvents.pointerUpHandler);
        }

        // Rotate left
        if (Input.GetKeyDown(_rLeftKey))
        {
            ExecuteEvents.Execute(left.gameObject, pointer, ExecuteEvents.pointerDownHandler);
        }
        if (Input.GetKeyUp(_rLeftKey))
        {
            ExecuteEvents.Execute(left.gameObject, pointer, ExecuteEvents.pointerUpHandler);
        }

        // Move up
        if (Input.GetKeyDown(_vUpKey))
        {
            ExecuteEvents.Execute(up.gameObject, pointer, ExecuteEvents.pointerDownHandler);
        }
        if (Input.GetKeyUp(_vUpKey))
        {
            ExecuteEvents.Execute(up.gameObject, pointer, ExecuteEvents.pointerUpHandler);
        }

        // Move down
        if (Input.GetKeyDown(_vDownKey))
        {
            ExecuteEvents.Execute(down.gameObject, pointer, ExecuteEvents.pointerDownHandler);
        }
        if (Input.GetKeyUp(_vDownKey))
        {
            ExecuteEvents.Execute(down.gameObject, pointer, ExecuteEvents.pointerUpHandler);
        }

        // Move out
        if (Input.GetKeyDown(_hOutKey))
        {
            ExecuteEvents.Execute(outwards.gameObject, pointer, ExecuteEvents.pointerDownHandler);
        }
        if (Input.GetKeyUp(_hOutKey))
        {
            ExecuteEvents.Execute(outwards.gameObject, pointer, ExecuteEvents.pointerUpHandler);
        }

        // Move in
        if (Input.GetKeyDown(_hInKey))
        {
            ExecuteEvents.Execute(inwards.gameObject, pointer, ExecuteEvents.pointerDownHandler);
        }
        if (Input.GetKeyUp(_hInKey))
        {
            ExecuteEvents.Execute(inwards.gameObject, pointer, ExecuteEvents.pointerUpHandler);
        }

        // Toggle pump
        if (Input.GetKeyDown(_pumpKey))
        {
            pump.isOn = !pump.isOn;
        }
        // Togle valve
        if (Input.GetKeyDown(_valveKey))
        {
            valve.isOn = !valve.isOn;
        }

        // Execute safe move with the right values
        if (_rotationPace != 0 || _verticalPace != 0 || _horizontalPace != 0)
        {
            _simulation.SafeArm3DMove(10, _horizontalPace, _outward, 10, _verticalPace, _down, 10, _rotationPace, _left);      
        }
    }

    // Methods to be used by the UI Buttons "Freie Bewegung"
    public void StartRotateLeft()
    {
        _rotationPace = _pace;
        _left = true;
    }
    public void StopRotateLeft()
    {
        _rotationPace = 0;
    }
    public void StartRotateRight()
    {
        _rotationPace = _pace;
        _left = false;
    }
    public void StopRotateRight()
    {
        _rotationPace = 0;
    }
    public void StartHorizontalOut()
    {
        _horizontalPace = _pace;
        _outward = true;
    }
    public void StopHorizontalOut()
    {
        _horizontalPace = 0;
    }
    public void StartHorizontalIn()
    {
        _horizontalPace = _pace;
        _outward = false;
    }
    public void StoptHorizontalIn()
    {
        _horizontalPace = 0;
    }
    public void StartVerticalDown()
    {
        _verticalPace = _pace;
        _down = true;

    }
    public void StopVerticalDown()
    {
        _verticalPace = 0;
    }
    public void StartVerticalUP()
    {
        _verticalPace = _pace;
        _down = false;
    }
    public void StopVerticalUP()
    {
        _verticalPace = 0;
    }    
}
