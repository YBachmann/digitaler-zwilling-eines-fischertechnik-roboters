﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Controller;
using UnityEngine.UI;
using TMPro;
public class Simulation : MonoBehaviour
{
    // Fields for activating and deactivating the ui elements
    [SerializeField]
    private GameObject h_pace;
    [SerializeField]
    private GameObject h_ticks;
    [SerializeField]
    private Toggle h_out;

    [SerializeField]
    private GameObject v_pace;
    [SerializeField]
    private GameObject v_ticks;
    [SerializeField]
    private Toggle v_down;

    [SerializeField]
    private GameObject r_pace;
    [SerializeField]
    private GameObject r_ticks;
    [SerializeField]
    private Toggle r_left;

    [SerializeField]
    private Button run;
    [SerializeField]
    private Button grip;
    [SerializeField]
    private Button release;
    [SerializeField]
    private Button homing;
    [SerializeField]
    private Button parking;

    [SerializeField]
    private Button inwards;
    [SerializeField]
    private Button outwards;
    [SerializeField]
    private Button up;
    [SerializeField]
    private Button down;
    [SerializeField]
    private Button left;
    [SerializeField]
    private Button right;
    [SerializeField]
    private Toggle pump;
    [SerializeField]
    private Toggle vale;


    [SerializeField]
    TxtController _txtController;
    private bool _lastRLeft;
    private bool _lastVDown;
    private bool _lastHOut;

    private RoboStatus _roboStatus;
    // Start is called before the first frame update
    void Start()
    {
        _roboStatus = RoboStatus.Instance;
    }

    private void Update()
    {
        if (!_roboStatus.GetOnlineStatus())
        {
            List<Dictionary<string, int>> motorInstructions = new List<Dictionary<string, int>>();
            if (_roboStatus.GetTriggerStatus(1) && !_lastRLeft) // Todo: Add overextending trigger
            {
                motorInstructions.Add(new Dictionary<string, int>(){
                    {"motorID", 1},
                    {"commandID", 0 },
                    {"pace", 0},
                    {"ticks", 0 } // Update ticks only if there is a new command ID
                });
            }
            if (_roboStatus.GetTriggerStatus(2) && !_lastVDown)// Todo: Add overextending trigger
            {
                motorInstructions.Add(new Dictionary<string, int>(){
                    {"motorID", 2},
                    {"commandID", 0 },
                    {"pace", 0},
                    {"ticks", 0 } // Update ticks only if there is a new command ID
                });
            }
            if (_roboStatus.GetTriggerStatus(3) && !_lastHOut)// Todo: Add overextending trigger
            {
                motorInstructions.Add(new Dictionary<string, int>(){
                    {"motorID", 3},
                    {"commandID", 0 },
                    {"pace", 0},
                    {"ticks", 0 } // Update ticks only if there is a new command ID
                });
            }

            if (_roboStatus.GetOverExtendStatus(1) && _lastRLeft)
            {
                motorInstructions.Add(new Dictionary<string, int>(){
                    {"motorID", 1},
                    {"commandID", 0 },
                    {"pace", 0},
                    {"ticks", 0 } // Update ticks only if there is a new command ID
                });
            }
            if (_roboStatus.GetOverExtendStatus(2) && _lastVDown)
            {
                motorInstructions.Add(new Dictionary<string, int>(){
                    {"motorID", 2},
                    {"commandID", 0 },
                    {"pace", 0},
                    {"ticks", 0 } // Update ticks only if there is a new command ID
                });
            }
            if (_roboStatus.GetOverExtendStatus(3) && _lastHOut)
            {
                motorInstructions.Add(new Dictionary<string, int>(){
                    {"motorID", 3},
                    {"commandID", 0 },
                    {"pace", 0},
                    {"ticks", 0 } // Update ticks only if there is a new command ID
                });
            }
            _roboStatus.UpdateMotorInstruction(motorInstructions);
        }
    }

    /// <summary>
    /// Move the Arms of the Robot.
    /// </summary>
    /// <param name="h_distance"></param>
    /// <param name="h_pace"></param>
    /// <param name="h_out"></param>
    /// <param name="v_distance"></param>
    /// <param name="v_pace"></param>
    /// <param name="v_down"></param>
    /// <param name="r_distance"></param>
    /// <param name="r_pace"></param>
    /// <param name="r_left"></param>
    public void Arm3DMovement(int h_distance, int h_pace, bool h_out,
                              int v_distance, int v_pace, bool v_down,
                              int r_distance, int r_pace, bool r_left)
    {        
        List<Dictionary<string, int>> motorInstructions = new List<Dictionary<string, int>>();
        motorInstructions.Add(new Dictionary<string, int>(){
            {"motorID", 1},
            {"commandID", 0 },
            {"pace", r_left? r_pace : -1*r_pace},
            {"ticks", r_distance } // Update ticks only if there is a new command ID
        });
        motorInstructions.Add(new Dictionary<string, int>(){
            {"motorID", 2},
            {"commandID", 0 },
            {"pace", v_down? v_pace : -1*v_pace},
            {"ticks", v_distance } // Update ticks only if there is a new command ID
        });
        motorInstructions.Add(new Dictionary<string, int>(){
            {"motorID", 3},
            {"commandID", 0 },
            {"pace", h_out? h_pace : -1*h_pace},
            {"ticks", h_distance } // Update ticks only if there is a new command ID
        });
        _roboStatus.UpdateMotorInstruction(motorInstructions);
    }

    /// <summary>
    /// Move the arms of the Robot but stop at sensor inputs.
    /// Works only in Offline Mode
    /// </summary>
    /// <param name="h_distance"></param>
    /// <param name="h_pace"></param>
    /// <param name="h_out"></param>
    /// <param name="v_distance"></param>
    /// <param name="v_pace"></param>
    /// <param name="v_down"></param>
    /// <param name="r_distance"></param>
    /// <param name="r_pace"></param>
    /// <param name="r_left"></param>
    public void SafeArm3DMove(int h_distance, int h_pace, bool h_out,
                              int v_distance, int v_pace, bool v_down,
                              int r_distance, int r_pace, bool r_left)
    {
        // check if the the Simulation is set to Offline
        if (!_roboStatus.GetOnlineStatus())
        {
            // Check if the Sensors are pressed
            if (_roboStatus.GetTriggerStatus(1) && !r_left)
            {
                r_pace = 0;
            }
            if (_roboStatus.GetTriggerStatus(2) && !v_down)
            {
                v_pace = 0;
            }
            if (_roboStatus.GetTriggerStatus(3) && !h_out)
            {
                h_pace = 0;
            }

            if (_roboStatus.GetOverExtendStatus(1) && r_left)
            {
                r_pace = 0;
            }
            if (_roboStatus.GetOverExtendStatus(2) && v_down)
            {
                v_pace = 0;
            }
            if (_roboStatus.GetOverExtendStatus(3) && h_out)
            {
                h_pace = 0;
            }

            _lastRLeft = r_left;
            _lastVDown = v_down;
            _lastHOut = h_out;
            Arm3DMovement(h_distance, h_pace, h_out, v_distance, v_pace, v_down, r_distance, r_pace, r_left);
        }
    }

    /// <summary>
    /// Update the state of the pump only in offline mode
    /// </summary>
    /// <param name="status"></param>
    public void SafeUpdatePump(bool status)
    {
        if (!_roboStatus.GetOnlineStatus())
        {
            _roboStatus.UpdateVacPumpState(status, 1);
        }
    }

    /// <summary>
    /// Update the state of the pump.
    ///  SHOULD ONLY BE USED IF NECESSERY!
    /// </summary>
    /// <param name="status"></param>
    public void ForceUpdatePump(bool status)
    {
  
        _roboStatus.UpdateVacPumpState(status, 1);
 
    }

    /// <summary>
    /// Update the state of the pump only in offline mode
    /// </summary>
    /// <param name="status"></param>
    public void SafeUpdateValve(bool status)
    {
        if (!_roboStatus.GetOnlineStatus())
        {
            _roboStatus.UpdateValveState(status, 1);
        }
    }

    /// <summary>
    /// Update the state of the pump.
    /// SHOULD ONLY BE USED IF NECESSERY!
    /// </summary>
    /// <param name="status"></param>
    public void ForceUpdateValve(bool status)
    {

        _roboStatus.UpdateValveState(status, 1);

    }

    /// <summary>
    /// Turn the virtual robot to offline mode
    /// </summary>
    public void GoOffline()
    {
        _txtController.ShutDownCommunication();
        _roboStatus.SetOnline(false);
    }

    /// <summary>
    /// Stop the running commands
    /// Stop all movement
    /// </summary>
    public void EmergencyBrake()
    {
        if (_roboStatus.GetOnlineStatus())
        {
            // stop online mode
            GoOffline();

            // Stop Pump if running
            SafeUpdatePump(false);
            SafeUpdateValve(false);
        }
        // stop movement
        Arm3DMovement(0, 0, false, 0, 0, false, 0, 0, false);        
    }

    /// <summary>
    /// Deactivate all UI Elements
    /// </summary>
    public void DeactivateAll()
    {
        Debug.Log("Deactivating All");
        h_pace.GetComponent<TMP_InputField>().interactable = false;
        h_ticks.GetComponent<TMP_InputField>().interactable = false;
        h_out.interactable = false;

        v_pace.GetComponent<TMP_InputField>().interactable = false;
        v_ticks.GetComponent<TMP_InputField>().interactable = false;
        v_down.interactable = false;

        r_pace.GetComponent<TMP_InputField>().interactable = false;
        r_ticks.GetComponent<TMP_InputField>().interactable = false;
        r_left.interactable = false;

        run.interactable = false;
        grip.interactable = false;
        release.interactable = false;
        homing.interactable = false;
        parking.interactable = false;

        inwards.interactable = false;
        outwards.interactable = false;
        up.interactable = false;
        down.interactable = false;
        left.interactable = false;
        right.interactable = false;
        pump.interactable = false;
        vale.interactable = false;
    }

    /// <summary>
    /// Activate all UI Elements
    /// </summary>
    public void ActivateAll()
    {
        Debug.Log("Activating All");
        h_pace.GetComponent<TMP_InputField>().interactable = true;
        h_ticks.GetComponent<TMP_InputField>().interactable = true;
        h_out.interactable = true;

        v_pace.GetComponent<TMP_InputField>().interactable = true; ;
        v_ticks.GetComponent<TMP_InputField>().interactable = true;
        v_down.interactable = true;

        r_pace.GetComponent<TMP_InputField>().interactable = true;
        r_ticks.GetComponent<TMP_InputField>().interactable = true;
        r_left.interactable = true;

        run.interactable = true;
        grip.interactable = true;
        release.interactable = true;
        homing.interactable = true;
        parking.interactable = true;

        inwards.interactable = true;
        outwards.interactable = true;
        up.interactable = true;
        down.interactable = true;
        left.interactable = true;
        right.interactable = true;
        pump.interactable = true;
        vale.interactable = true;
    }
}
