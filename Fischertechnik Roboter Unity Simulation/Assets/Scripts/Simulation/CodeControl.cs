using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Mock TXT controller. Just for testing. 
/// </summary>
/// <seealso cref="MonoBehaviour" />
public class CodeControl : MonoBehaviour
{   
    [SerializeField]
    Simulation _simulation;

    /// <summary>
    /// Input fields to read the values from
    /// </summary>
    [SerializeField]
    private GameObject _hPace;
    [SerializeField]
    private GameObject _hTicks;
    [SerializeField]
    private Toggle _hOut;

    [SerializeField]
    private GameObject _vPace;
    [SerializeField]
    private GameObject _vTicks;
    [SerializeField]
    private Toggle _vDown;

    [SerializeField]
    private GameObject _rPace;
    [SerializeField]
    private GameObject _rTicks;
    [SerializeField]
    private Toggle _rLeft;

    // Max values defined by the Java Programm. 
    private readonly int _maxHDistance = 950;
    private readonly int _maxVDistance = 880;
    private readonly int _maxRDdistance = 1336;
    private readonly int _maxPace = 512;
    private readonly int _minInputValue = 0;

    private RoboStatus _roboStatus;

    private void Start()
    {
        _roboStatus = RoboStatus.Instance;
    }

    /// <summary>
    /// This Method should simulate the use of the Java Method "Arm3DMove()" defined in the Java Programm.
    /// Reads values from the input fields and fills them if needed
    /// </summary>
    public void OnRunCommit()
    {
        int horizontalTicks = 0;
        int horizontalPace = 0;
        int verticalTicks = 0;
        int verticalPace = 0;
        int rotationalTicks = 0;
        int rotationalPace = 0;

        //Parse Values from Input fields

        // Horizontal Move
        try
        {            
            horizontalTicks = int.Parse(_hTicks.GetComponent<TMP_InputField>().text.ToString());
            if (horizontalTicks > _maxHDistance)
            {
                horizontalTicks = _maxHDistance;
                _hTicks.GetComponent<TMP_InputField>().text = horizontalTicks.ToString();
            }
            else if(horizontalTicks < _minInputValue)
            {
                horizontalTicks = _minInputValue;
                _hTicks.GetComponent<TMP_InputField>().text = horizontalTicks.ToString();
            }                
        }
        catch
        {
            _hTicks.GetComponent<TMP_InputField>().text = horizontalTicks.ToString();
        }

        try
        {
            horizontalPace = int.Parse(_hPace.GetComponent<TMP_InputField>().text.ToString());
            if (horizontalPace > _maxPace)
            {
                horizontalPace = _maxPace;
                _hPace.GetComponent<TMP_InputField>().text = horizontalPace.ToString();
            }
            else if (horizontalPace < _minInputValue)
            {
                horizontalPace = _minInputValue;
                _hPace.GetComponent<TMP_InputField>().text = horizontalPace.ToString();
            }
        }
        catch
        {
            _hPace.GetComponent<TMP_InputField>().text = horizontalPace.ToString();
        }

        // Vertical Move
        try
        {
            verticalTicks = int.Parse(_vTicks.GetComponent<TMP_InputField>().text.ToString());
            if (verticalTicks > _maxVDistance)
            {
                verticalTicks = _maxVDistance;
                _vTicks.GetComponent<TMP_InputField>().text = verticalTicks.ToString();
            }
            else if (verticalTicks < _minInputValue)
            {
                verticalTicks = _minInputValue;
                _vTicks.GetComponent<TMP_InputField>().text = verticalTicks.ToString();
            }
        }
        catch
        {
            _vTicks.GetComponent<TMP_InputField>().text = verticalTicks.ToString();
        }

        try
        {
            verticalPace = int.Parse(_vPace.GetComponent<TMP_InputField>().text.ToString());
            if (verticalPace > _maxPace)
            {
                verticalPace = _maxPace;
                _vPace.GetComponent<TMP_InputField>().text = verticalPace.ToString();
            }
            else if (verticalPace < _minInputValue)
            {
                verticalPace = _minInputValue;
                _vPace.GetComponent<TMP_InputField>().text = verticalPace.ToString();
            }
        }
        catch
        {
            _vPace.GetComponent<TMP_InputField>().text = verticalPace.ToString();
        }

        // Rotational Move
        try
        {
            rotationalTicks = int.Parse(_rTicks.GetComponent<TMP_InputField>().text.ToString());
            if (rotationalTicks > _maxRDdistance)
            {
                rotationalTicks = _maxRDdistance;
                _rTicks.GetComponent<TMP_InputField>().text = rotationalTicks.ToString();
            }
            else if (rotationalTicks < _minInputValue)
            {
                rotationalTicks = _minInputValue;
                _rTicks.GetComponent<TMP_InputField>().text = rotationalTicks.ToString();
            }
        }
        catch
        {
            _rTicks.GetComponent<TMP_InputField>().text = verticalTicks.ToString();
        }

        try
        {
            rotationalPace = int.Parse(_rPace.GetComponent<TMP_InputField>().text.ToString());
            if (rotationalPace > _maxPace)
            {
                rotationalPace = _maxPace;
                _rPace.GetComponent<TMP_InputField>().text = rotationalPace.ToString();
            }
            else if (rotationalPace < _minInputValue)
            {
                rotationalPace = _minInputValue;
                _rPace.GetComponent<TMP_InputField>().text = rotationalPace.ToString();
            }
        }
        catch
        {
            _rPace.GetComponent<TMP_InputField>().text = verticalPace.ToString();
        }     

        // for homing
        if (!_hOut.isOn && horizontalTicks == 0)
        {
            horizontalTicks = 2000;
        }
        
        if (!_vDown.isOn && verticalTicks == 0)
        {
            verticalTicks = 2000;
        }
        
        if (!_rLeft.isOn && rotationalTicks == 0)
        {
            rotationalTicks = 2000;
        }

        _simulation.SafeArm3DMove(horizontalTicks, horizontalPace, _hOut.isOn,
                                   verticalTicks, verticalPace, _vDown.isOn,
                                   rotationalTicks, rotationalPace, _rLeft.isOn);
    }

    /// <summary>
    /// This Method is mirroring a method found in the Java programm.
    /// Executes a grip routine, picking up a grippable object.
    /// </summary>
    public void Grip()
    {
        StartCoroutine(timedGripExecution());
        IEnumerator timedGripExecution()
        {
            _simulation.DeactivateAll();
            _simulation.SafeArm3DMove(0, 0, false, 50, 500, true, 0, 0, false);
            // wait for motor to finish instruction
            while (!MovesDone())
            {
                yield return new WaitForSeconds(0.01f);
            }
            _simulation.SafeUpdatePump(true);
            // wait 500 ms       
            yield return new WaitForSeconds(0.5f);
            _simulation.SafeUpdateValve(true);
            _simulation.SafeArm3DMove(0, 0, false, 50, 500, false, 0, 0, false);
            // wait for motor to finish instruction
            while (!MovesDone())
            {
                yield return new WaitForSeconds(0.01f);
            }
            _simulation.ActivateAll();
        }
    }

    /// <summary>
    /// This Method is mirroring a method found in the Java programm.
    /// Execute a release routine, defined by the Java-Program
    /// Releases the grippable object.
    /// </summary>
    public void Release()
    {
        StartCoroutine(timedReleaseExecution());

        IEnumerator timedReleaseExecution()
        {
            _simulation.DeactivateAll();
            _simulation.SafeArm3DMove(0, 0, false, 50, 400, true, 0, 0, false);
            // wait for motor to finish instruction
            while (!MovesDone())
            {
                yield return new WaitForSeconds(0.01f);
            }
            _simulation.SafeUpdatePump(false);
            _simulation.SafeUpdateValve(false);
            _simulation.SafeArm3DMove(0, 0, false, 50, 400, false, 0, 0, false);
            // wait for motor to finish instruction
            while (!MovesDone())
            {
                yield return new WaitForSeconds(0.01f);
            }
            _simulation.ActivateAll();
        }
    }

    /// <summary>
    /// This Method is mirroring a method found in the Java programm.
    /// Moves the axis towards the senors until every sensor is triggered.
    /// </summary>
    public void Homing()
    {
        StartCoroutine(timedHomingExecution());
        IEnumerator timedHomingExecution()
        {
            _simulation.DeactivateAll();
            _simulation.SafeArm3DMove(2000, 400, false, 2000, 400, false, 2000, 400, false);
            // wait for motor to finish instruction
            while (!MovesDone())
            {
                yield return new WaitForSeconds(0.01f);
            }
            _simulation.ActivateAll();
        }
    }

    /// <summary>
    /// This Method is mirroring a method found in the Java programm.
    /// Move the Arm to a specific point in Space.
    /// Because there is no way implemented to track the current position,
    /// this will only move to the right spot if homing() is called right before.
    /// </summary>
    public void Parking()
    {
        // Homing has to be called first, because there is nothing tracking the current position.
        Homing();
        StartCoroutine(timedParkingExecution());
    
        IEnumerator timedParkingExecution()
        {
            _simulation.DeactivateAll();
            _simulation.SafeArm3DMove(100, 400, true, 500, 500, true, 890, 500, true);
            // wait for motor to finish instruction
            while (!MovesDone())
            {
                yield return new WaitForSeconds(0.01f);
            }
            _simulation.ActivateAll();
        }
    }

    /// <summary>
    /// Returns true only when all motors stand still.
    /// </summary>
    /// <returns></returns>
    private bool MovesDone()
    {
        if(_roboStatus.GetMotorPace(1) == 0 
            && _roboStatus.GetMotorPace(2) == 0
            && _roboStatus.GetMotorPace(3) == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }    
}
