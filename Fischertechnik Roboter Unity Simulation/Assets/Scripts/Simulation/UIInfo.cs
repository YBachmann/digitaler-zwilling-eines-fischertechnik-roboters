using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using Newtonsoft.Json;

public class UIInfo : MonoBehaviour, IEventListener
{
    [SerializeField]
    private Simulation _simulation;

    // For Online Status
    [SerializeField]
    private TextMeshProUGUI _onlineStatus;

    //for UI LiveData page
    [SerializeField]
    private TextMeshProUGUI _rotationPace;
    [SerializeField]
    private TextMeshProUGUI _verticalPace;
    [SerializeField]
    private TextMeshProUGUI _horizontalPace;

    [SerializeField]
    private Image _pumpStatus;
    [SerializeField]
    private Image _valveStatus;

    [SerializeField]
    private Image _rotSensorStatus;
    [SerializeField]
    private Image _vertSensorStatus;
    [SerializeField]
    private Image _horiSensorStatus;

    // For Logger page
    [SerializeField]
    private TMP_Dropdown _loglevel;
    [SerializeField]
    private GameObject _logTextTemplate;


    // Definitions for colors.
    private Color32 _greenColor = new Color32(0x77, 0xB4, 0x70, 0xFF);
    private Color32 _redColor = new Color32(0xE5, 0x5C, 0x53, 0xFF);
    private Color32 _whiteColor = new Color32(0xFF, 0xFF, 0xFF, 0xFF);

    // Definitions to be used when reading robot-status.
    private readonly int _rotationMotorId = 1;
    private readonly int _verticalMotorId = 2;
    private readonly int _horizontalMotorId = 3;
    private readonly int _pumpMotorId = 4;
    private readonly int _valveMotorId = 5;
    private readonly int _rotationSensorId = 1;
    private readonly int _verticalSensorId = 2;
    private readonly int _horizontalSensorId = 3;
    private readonly string _turnArrowLeft = "<sprite=7>";
    private readonly string _turnArrowRight = "<sprite=6>";

    // Stores log texts
    private List<Dictionary<string, string>> _logTextList;
    private List<GameObject> _logTextItems;
    // Used so the Online status doesnt update every frame
    private bool _online = false;

    private Logger _logger;
    private RoboStatus _roboStatus;

    private void Start()
    {
        _roboStatus = RoboStatus.Instance;
        _logger = Logger.Instance;
        _logTextList = new List<Dictionary<string, string>>();
        _logTextItems = new List<GameObject>();

        _loglevel.ClearOptions();
        List<string> loglevels = new List<string>(Enum.GetNames(typeof(LogLevel)));        
        _loglevel.AddOptions(loglevels);
        _loglevel.value = (int)_logger.LogLevelGlobal;

        _roboStatus.eventPublisher.subscribe(EventPublisher.EventType.UiLog, this);
    }

    // Update is called once per frame
    void Update()
    {
        // Display the online Status of the robot
        if (_roboStatus.GetOnlineStatus() && !_online)
        {
            _online = true;
            _onlineStatus.text = "Onlinemodus";
            _onlineStatus.color = _greenColor;

            _simulation.DeactivateAll();
        }
        else if (!_roboStatus.GetOnlineStatus() && _online)
        {
            _online = false;
            _onlineStatus.text = "Offlinemodus";
            _onlineStatus.color = _redColor;

            _simulation.ActivateAll();
        }

        string jsonData = _roboStatus.GetUiData();
        // Debug.Log(jsonData);
        List<List<Dictionary<string, int>>> uIData = JsonConvert.DeserializeObject<List<List<Dictionary<string, int>>>>(jsonData);

        // Motor Info
        foreach (Dictionary<string, int> listElement in uIData[0])
        {
            try
            {
                // Display rotation motor speed
                if (listElement["motorID"] == _rotationMotorId)
                {
                    int pace = listElement["pace"];
                    string text = pace.ToString();
                    if (pace > 0)
                    {
                        text = text + _turnArrowLeft;
                    }
                    else if (pace < 0)
                    {
                        text = text + _turnArrowRight;
                    }
                    _rotationPace.text = text;
                }
                // Display vertical motor speed
                if (listElement["motorID"] == _verticalMotorId)
                {
                    int pace = listElement["pace"];
                    string text = pace.ToString();
                    if (pace > 0)
                    {
                        text = text + _turnArrowLeft;
                    }
                    else if (pace < 0)
                    {
                        text = text + _turnArrowRight;
                    }
                    _verticalPace.text = text;
                }
                // Display horizontal motor speed
                if (listElement["motorID"] == _horizontalMotorId)
                {
                    int pace = listElement["pace"];
                    string text = pace.ToString();
                    if (pace > 0)
                    {
                        text = text + _turnArrowLeft;
                    }
                    else if (pace < 0)
                    {
                        text = text + _turnArrowRight;
                    }
                    _horizontalPace.text = text;
                }
            }
            catch (KeyNotFoundException e)
            {
                Debug.Log("This key is invalid" + e);
                continue;
            }
        }

        // Pump and Valve Status
        foreach (Dictionary<string, int> listElement in uIData[1])
        {
            try
            {
                // Check if the Pump is active
                if (listElement["motorID"] == _pumpMotorId)
                {
                    int status = listElement["active"];
                    if (status == 1)
                    {
                        _pumpStatus.color = _greenColor;
                    }
                    else
                    {
                        _pumpStatus.color = _whiteColor;
                    }

                }
                // Check if the Valve is active
                if (listElement["motorID"] == _valveMotorId)
                {
                    int status = listElement["active"];
                    if (status == 1)
                    {
                        _valveStatus.color = _greenColor;
                    }
                    else
                    {
                        _valveStatus.color = _whiteColor;
                    }

                }
            }
            catch (KeyNotFoundException e)
            {
                Debug.Log("This key is invalid" + e);
                continue;
            }
        }

        // Display Sensor Data
        foreach (Dictionary<string, int> listElement in uIData[2])
        {
            try
            {
                // Check if the rotation sensor is pressed
                if (listElement["sensorID"] == _rotationSensorId)
                {
                    int status = listElement["active"];
                    if (status == 1)
                    {
                        _rotSensorStatus.color = _greenColor;
                    }
                    else
                    {
                        _rotSensorStatus.color = _whiteColor;
                    }

                }
                // Check if the vertical sensor is pressed
                if (listElement["sensorID"] == _verticalSensorId)
                {
                    int status = listElement["active"];
                    if (status == 1)
                    {
                        _vertSensorStatus.color = _greenColor;
                    }
                    else
                    {
                        _vertSensorStatus.color = _whiteColor;
                    }

                }
                // Check if the horizontal sensor is pressed
                if (listElement["sensorID"] == _horizontalSensorId)
                {
                    int status = listElement["active"];
                    if (status == 1)
                    {
                        _horiSensorStatus.color = _greenColor;
                    }
                    else
                    {
                        _horiSensorStatus.color = _whiteColor;
                    }

                }

            }
            catch (KeyNotFoundException e)
            {
                Debug.Log("This key is invalid" + e);
                continue;
            }
        }

        // Add log messages        
        if(_logTextList.Count > 0)
        {
            AddDisplayText(_logTextList[0]["text"], (LogLevel)Int32.Parse(_logTextList[0]["loglevel"]));
        }       
        _logTextList.Clear();
    }

    /// <summary>
    /// Method to be calle when a log level is chosen
    /// </summary>
    /// <param name="loglevel"></param>
    public void OnLoglevelChosen(int loglevel)
    {
        _logger.SetLoglevel((LogLevel)loglevel);
    }

    /// <summary>
    /// Add a Message to the log list. 
    /// Also displays an icon depending on the loglevel.
    /// </summary>
    /// <param name="text"></param>
    /// <param name="loglevel"></param>
    private void AddDisplayText(string text, LogLevel loglevel)
    {
        int offset = 9;
        string displaytext = "<sprite=" + (offset + (int)loglevel) + "> " + text;

        if (_logTextItems.Count >= 50)
        {
            GameObject tempItem = _logTextItems[0];
            Destroy(tempItem.gameObject);
            _logTextItems.Remove(tempItem);
        }

        GameObject newText = Instantiate(_logTextTemplate) as GameObject;
        newText.SetActive(true);
        newText.GetComponent<TextMeshProUGUI>().SetText(displaytext);
        newText.transform.SetParent(_logTextTemplate.transform.parent, false);

        _logTextItems.Add(newText.gameObject);
    }

    /// <summary>
    /// Receive calls from everywhere to send log messages to a .txt-file. 
    /// </summary>
    /// <param name="ui_data">List of dictionaries from type string/string, where receiving the data from EventPublisher</param>
    /// <param name="_uiLogList">global list of dictionaries from type string/string</param>
    public void UpdateListener(string jsonData)
    {
        // convert json back to list of dicts
        List<Dictionary<string, string>> ui_data = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(jsonData);
        _logTextList.Add(ui_data[0]);
    }
}
