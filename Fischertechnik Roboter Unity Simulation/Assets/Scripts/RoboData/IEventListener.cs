using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEventListener
{
    /// <summary>
    /// Receive update from EventPublisher
    /// </summary>
    /// <param name="jsonData">Data in JSON format</param>
    public void UpdateListener(string jsonData);
}
