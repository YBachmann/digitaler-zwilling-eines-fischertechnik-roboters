using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EventPublisher
{
    /// Event types that can be registered and notified. 
    public enum EventType
    {
        MotorInstruction,
        VacPumpInstruction,
        ValveInstruction,
        UiLog
    }

    /// This Dictionary contains a key for each possible EventType and a list of listeners that subscribed to that event. 
    private Dictionary<EventType, List<IEventListener>> _listenersByEventType = new Dictionary<EventType, List<IEventListener>>();

    private Logger _logger;

    public EventPublisher()
    {
        _logger = Logger.Instance;

        // initialize dictionary containing a listener-list for each eventtype
        foreach (EventType e in Enum.GetValues(typeof(EventType)))
        {
            _listenersByEventType.Add(e, new List<IEventListener>());
        }

        // log the EventTypes that were used to initialize '_listenersByEventType'
        string msg = "EventPublisher initialized with EventTypes: '";
        foreach (var item in Enum.GetValues(typeof(EventType)))
        {
            msg += item.ToString() + ", ";
        }
        // _logger.CombinedLog(msg, LogLevel.Debug);
    }

    /// <summary>
    /// Subscribe a listener to a specific EventType. 
    /// </summary>
    /// <param name="eventType">The EventType that the listener wants to subscribe to. </param>
    /// <param name="listener">The listener object.</param>
    public void subscribe(EventType eventType, IEventListener listener)
    {
        List<IEventListener> listenerList;
        // Try to get the list that the listener should be added to. 
        if (_listenersByEventType.TryGetValue(eventType, out listenerList) == false)
        {
            _logger.CombinedLog("Can't subscribe listener '" + listener.ToString() + "' to eventType '" + eventType.ToString()
                        + "'. EventType not found in '_listenersByEventType'. ", LogLevel.Error);
            return;
        }
        // Check if the listener is already subscribed to the list. 
        if (listenerList.Contains(listener))
        {
            _logger.CombinedLog("Listener '" + listener.ToString() + "' is already subscribed to eventType '" + eventType.ToString() + "'. ", LogLevel.Warn);
            return;
        }

        // Add the listener to the corresponding listener list if none of the above error-checks failed. 
        listenerList.Add(listener);
        _logger.CombinedLog("Listener '" + listener.ToString() + "' subscribed to eventType '" + eventType.ToString() + "'. ", LogLevel.Info);
    }

    /// <summary>
    /// Unsubscribe a listener from a specific EventType. 
    /// </summary>
    /// <param name="eventType">The EventType that the listener wants to unsubscribe from. </param>
    /// <param name="listener">The listener object.</param>
    public void unsubscribe(EventType eventType, IEventListener listener)
    {
        List<IEventListener> listenerList;
        // Try to get the list that the listener should be removed from. 
        if (_listenersByEventType.TryGetValue(eventType, out listenerList) == false)
        {
            _logger.CombinedLog("Can't unsubscribe from eventType '" + eventType.ToString()
                        + "'. EventType not found in '_listenersByEventType'. ", LogLevel.Error);
            return;
        }

        // Remove the listener from the corresponding listener list. 
        listenerList.Remove(listener);
        _logger.CombinedLog("Listener '" + listener.ToString() + "' unsubscribed from event '" + eventType.ToString() + "'. ", LogLevel.Info);
    }

    /// <summary>
    /// Notify all listeners of a specific EventType. 
    /// </summary>
    /// <param name="eventType">The EventType whose listeners should be notified.</param>
    /// <param name="jsonData">The Data that should be sent to the listeners.</param>
    public void notify(EventType eventType, string jsonData)
    {
        List<IEventListener> listenerList;
        // Try to get the list of listeners of the specified EventType
        if (_listenersByEventType.TryGetValue(eventType, out listenerList) == false)
        {
            _logger.CombinedLog("Can't notify listeners of eventType '" + eventType.ToString()
                        + "'. EventType not found in '_listenersByEventType'. ", LogLevel.Error);
            return;
        }
        // Update all listeners
        foreach (IEventListener listener in listenerList)
        {
            listener.UpdateListener(jsonData);
        }
    }
}