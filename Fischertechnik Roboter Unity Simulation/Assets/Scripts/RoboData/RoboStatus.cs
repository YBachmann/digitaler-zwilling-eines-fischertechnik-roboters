using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System;

public sealed class RoboStatus : SingletonBase<RoboStatus>
{
    public EventPublisher eventPublisher { get; private set; }

    private List<Motor> _motors = new List<Motor>();
    private List<VacPump> _vacPumps = new List<VacPump>();
    private List<Valve> _valves = new List<Valve>();
    private Dictionary<int, bool> _triggerStatus = new Dictionary<int, bool>();
    private Dictionary<int, int> _motorCommandID = new Dictionary<int, int>();
    private Dictionary<int, bool> _overExtendStatus = new Dictionary<int, bool>();
    private bool _isOnline;



    private RoboStatus()
    {
        eventPublisher = new EventPublisher();
        _triggerStatus = new Dictionary<int, bool> { { 1, false }, { 2, false }, { 3, false }, { 4, false } }; //TODO: this should be initialized with real trigger values
        _motorCommandID = new Dictionary<int, int> { { 1, 0 }, { 2, 0 }, { 3, 0 } };
        _overExtendStatus = new Dictionary<int, bool> { { 1, false }, { 2, false }, { 3, false } };
        _isOnline = false;
    }

    /// <summary>
    /// Registers a motor in order to beeing able to read its data. 
    /// </summary>
    /// <param name="motor">The motor object to register.</param>
    public void RegisterMotor(Motor motor)
    {
        if (_motors.Contains(motor))
        {
            Logger.Instance.CombinedLog("Motor '" + motor.gameObject.name + "' is already registered. ", LogLevel.Warn);
            return;
        }

        _motors.Add(motor);
    }

    /// <summary>
    /// Send instructions to all or some motors via the observer pattern. 
    /// </summary>
    /// <param name="MotorInstructions">The motor instructions. 
    /// If you don't want to move all motors just use "pace":0 and "ticks":0 for the motor that shouldn't rotate. </param>
    public void UpdateMotorInstruction(List<Dictionary<string, int>> MotorInstructions)
    {
        // send motor instructions to event publisher
        string jsonData = JsonConvert.SerializeObject(MotorInstructions);
        eventPublisher.notify(EventPublisher.EventType.MotorInstruction, jsonData);

        foreach (Dictionary<string, int> listElement in MotorInstructions)
            try
            {
                SetMotorCommandId(listElement["motorID"], listElement["commandID"]);
            }
            catch
            {
                Logger.Instance.CombinedLog("You tried to write a command without an id", LogLevel.Error);
            }
    }

    /// <summary>
    /// Return the pace of a motor with a given motorId. 
    /// </summary>
    /// <param name="motorID">The motorId of the motor whose pace should be returned.</param>
    public float GetMotorPace(int motorID)
    {
        Motor motor = _motors.Find(item => item.MotorID == motorID);
        if (!motor)
        {
            Logger.Instance.CombinedLog("Cannot get pace of motor with MotorID=" + motorID + " because this motor is not registered. ", LogLevel.Error);
            return 0f;
        }
        return motor.Pace;
    }

    /// <summary>
    /// Return the remaining ticks of a motor with a given motorId. 
    /// </summary>
    /// <param name="motorID">The motorId of the motor whose remaining ticks should be returned.</param>
    public float GetMotorRemainingTicks(int motorID)
    {
        Motor motor = _motors.Find(item => item.MotorID == motorID);
        if (!motor)
        {
            Logger.Instance.CombinedLog("Cannot get remaining ticks of motor with MotorID=" + motorID + " because this motor is not registered. ", LogLevel.Error);
            return 0f;
        }
        return motor.RemainingTicks;
    }

    public void SetTriggerStatus(int triggerID, bool status)
    {
        _triggerStatus[triggerID] = status;
    }

    public bool GetTriggerStatus(int triggerID)
    {
        return _triggerStatus[triggerID];
    }
    public void SetMotorCommandId(int motorId, int id)
    {
        _motorCommandID[motorId] = id;
    }

    public int GetMotorCommandId(int motorId)
    {
        return _motorCommandID[motorId];
    }

    public void SetOnline(bool status)
    {
        _isOnline = status;
    }
    public bool GetOnlineStatus()
    {
        return _isOnline;
    }

    /// <summary>
    /// Registers a VacPump in order to beeing able to read its data. 
    /// </summary>
    /// <param name="vacPump">The VacPump object to register.</param>
    public void RegisterVacPump(VacPump vacPump)
    {
        if (_vacPumps.Contains(vacPump))
        {
            Logger.Instance.CombinedLog("VacPump '" + vacPump.gameObject.name + "' is already registered. ", LogLevel.Warn);
            return;
        }

        _vacPumps.Add(vacPump);
    }

    /// <summary>
    /// Updates vac pump state.
    /// </summary>
    /// <param name="running">Wether the pump should be running.</param>
    public void UpdateVacPumpState(bool running, int vacPumpID = VacPump.DefaultVacPumpId)
    {
        string jsonData = JsonConvert.SerializeObject(new System.Tuple<bool, int>(running, vacPumpID));
        eventPublisher.notify(EventPublisher.EventType.VacPumpInstruction, jsonData);
    }

    /// <summary>
    /// Gets vac pump state.
    /// </summary>
    /// <param name="vacPumpID">The vac pump ID.</param>
    public bool GetVacPumpState(int vacPumpID = VacPump.DefaultVacPumpId)
    {
        VacPump vacPump = _vacPumps.Find(item => item.VacPumpId == vacPumpID);
        if (!vacPump)
        {
            Logger.Instance.CombinedLog("Cannot get state of pump with VacPumpID=" + vacPumpID + " because this pump is not registered. ", LogLevel.Error);
            return false;
        }
        return vacPump.Active;
    }

    /// <summary>
    /// Registers a valve in order to beeing able to read its data. 
    /// </summary>
    /// <param name="valve">The valve object to register.</param>
    public void RegisterValve(Valve valve)
    {
        if (_valves.Contains(valve))
        {
            Logger.Instance.CombinedLog("valve '" + valve.gameObject.name + "' is already registered. ", LogLevel.Warn);
            return;
        }

        _valves.Add(valve);
    }

    /// <summary>
    /// Updates valve state. 
    /// 
    /// 'closed' means that air can flow from connector 'A' to 'B'. 
    /// 'not closed' means that connector 'B' is connected to the air vent. 
    /// </summary>
    /// <param name="closed">Wether the valve should be closed. </param>
    public void UpdateValveState(bool closed, int valveID = Valve.DefaultValveId)
    {
        string jsonData = JsonConvert.SerializeObject(new System.Tuple<bool, int>(closed, valveID));
        eventPublisher.notify(EventPublisher.EventType.ValveInstruction, jsonData);
    }

    /// <summary>
    /// Gets valve state.
    /// 'closed' means that air can flow from connector 'A' to 'B'. 
    /// 'not closed' means that connector 'B' is connected to the air vent. 
    /// </summary>
    /// <param name="valveID">The valve i d.</param>
    public bool IsValveStateClosed(int valveID = Valve.DefaultValveId)
    {
        Valve valve = _valves.Find(item => item.ValveId == valveID);
        if (!valve)
        {
            Logger.Instance.CombinedLog("Cannot get state of pump with valveID=" + valveID + " because this pump is not registered. ", LogLevel.Error);
            return false;
        }
        return valve.Closed;
    }


    /// <summary>
    /// Send data from Logger to UI via observer pattern. 
    /// </summary>
    /// <param name="UiLog">The log messages from each user of the UiLog Method from Logger</param>
    public void updateUiLog(List<Dictionary<string, string>> UiLog)
    {
        //send UiLogs to event publisher
        string jsonData = JsonConvert.SerializeObject(UiLog);
        eventPublisher.notify(EventPublisher.EventType.UiLog, jsonData);
        //wird von Logger aufgerufen!
    }

    /// <summary>
    /// Receive current date to UI when called. 
    /// </summary>
    /// <param name="motorSpeed">List of data from each of the 3 main motors</param>
    /// <param name="motorActive">List of data from the vacuumpumpmotor and the valvemotor</param>
    /// <param name="sensorStatus">List of data from the 3 sensors</param>
    /// <param name="UiData">List of all 3 lists above</param>
    /// <param name="jsonUiData">Jsonobject includes all data and get send to UI</param>
    public string GetUiData()
    {
        List<Dictionary<string, int>> motorSpeed = new List<Dictionary<string, int>>();
        motorSpeed.Add(new Dictionary<string, int>(){
            {"motorID", 1},
            {"pace", (int)GetMotorPace(1)}
        });
        motorSpeed.Add(new Dictionary<string, int>(){
            {"motorID", 2},
            {"pace", (int)GetMotorPace(2)}
        });
        motorSpeed.Add(new Dictionary<string, int>(){
            {"motorID", 3},
            {"pace", (int)GetMotorPace(3)}
        });

        List<Dictionary<string, int>> motorActive = new List<Dictionary<string, int>>();
        motorActive.Add(new Dictionary<string, int>(){
            {"motorID", 4},     //Vacuumpump
            {"active", Convert.ToInt32(GetVacPumpState(1))}       //False = 0, True = 1
        });
        motorActive.Add(new Dictionary<string, int>(){
            {"motorID", 5},     //Valve
            {"active", Convert.ToInt32(IsValveStateClosed(1))}       //False = 0, True = 1
        });

        List<Dictionary<string, int>> sensorStatus = new List<Dictionary<string, int>>();
        sensorStatus.Add(new Dictionary<string, int>(){
            {"sensorID", 1},
            {"active", Convert.ToInt32(GetTriggerStatus(1))}
        });
        sensorStatus.Add(new Dictionary<string, int>(){
            {"sensorID", 2},
            {"active", Convert.ToInt32(GetTriggerStatus(2))}
        });
        sensorStatus.Add(new Dictionary<string, int>(){
            {"sensorID", 3},
            {"active", Convert.ToInt32(GetTriggerStatus(3))}
        });

        List<object> UiData = new List<object>();
        UiData.Add(motorSpeed);
        UiData.Add(motorActive);
        UiData.Add(sensorStatus);

        string jsonUiData = JsonConvert.SerializeObject(UiData);
        return jsonUiData;
    }

    public bool GetOverExtendStatus(int overExtendID)
    {
        return _overExtendStatus[overExtendID];
    }
    public void SetOverExtendStatus(int overExtendID, bool status)
    {
        _overExtendStatus[overExtendID] = status;
    }
}
