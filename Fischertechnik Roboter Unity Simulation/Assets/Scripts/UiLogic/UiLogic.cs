using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using Newtonsoft.Json;

/// <summary>
/// Class, which contains the logic from the user interface. Contains the subscribe logic for the EventPublisher as well.
/// </summary>
/// <param name="_uiLogList">global list of dictionaries from type string/string</param>
public class UiLogic: MonoBehaviour, IEventListener
{

    [SerializeField]
    private List<Dictionary<string, string>> _uiLogList = new List<Dictionary<string, string>>();

    /// <summary>
    /// Start method get used by Unity at the beginning.
    /// </summary>
    void Start()
    {
        RoboStatus.Instance.eventPublisher.subscribe(EventPublisher.EventType.UiLog, this);
    }

    void Update()
    {
        
    }

    /// <summary>
    /// Receive calls from everywhere to send log messages to a .txt-file. 
    /// </summary>
    /// <param name="ui_data">List of dictionaries from type string/string, where receiving the data from EventPublisher</param>
    /// <param name="_uiLogList">global list of dictionaries from type string/string</param>
    public void UpdateListener(string jsonData)
    {
        // convert json back to list of dicts
        List<Dictionary<string, string>> ui_data = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(jsonData);

        if(_uiLogList.Count >= 100)
        {
            _uiLogList.RemoveRange(0, 1);
        }

        
        _uiLogList.Add(new Dictionary<string, string>()
        {
            {"loglevel", ui_data[0]["loglevel"]},
            {"text", ui_data[0]["text"]}
        });

        String printList = "";
        foreach(Dictionary<string, string> listElement in _uiLogList)
        {
            printList += listElement["loglevel"] + ": " + listElement["text"];
        }
    }
}